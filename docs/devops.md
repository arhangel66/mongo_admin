# DevOps


## Table of Contents

* [Deployment](#deployment)
    * [tranquil.newagelabs.co](#bare-bones)
        * [Frontend](#bb-frontend)
        * [Backend](#bb-backend)
        * [Frontend and backend](#bb-frontend-and-backend)
    * [ma-admin.herokuapp.com](#heroku)
* [Local Docker Setup](#local-docker)
    * [Replicate MongoDB](#replicate-mongo)
    

## <a name="deployment"></a>Deployment

### <a name="bare-bones"></a>tranquil.newagelabs.co

1. SSH into the instance:
	```bash
	ssh -i <ubuntu_id_rsa path> django@138.197.198.253
	```

2. Pull:
	```bash
	git pull
	```

#### <a name="bb-frontend"></a>Fronted only

3. Update `fill_forms_irs` submodule if needed:
	```bash
	git submodule update fill_forms_irs
	```

4. Collect static files if needed:
	```bash
	python manage.py collectstatic \
		--no-input \
		--clear \
		--settings config.settings.production
	```

5. Restart the web server:
	 ```bash
	nohup python manage.py runserver --settings=config.settings.production 0.0.0.0:8004 &
	# then, press <Ctrl> + <C>
	```
	
#### <a name="bb-backend"></a>Backend only

3. Update dependencies if needed:
	```bash
	pip install -r requirements/production.txt
	```

4. Run migrations if any:
	```bash
	python manage.py migrate --settings config.settings.production
	```

5. Restart the web server:
	 ```bash
	nohup python manage.py runserver --settings=config.settings.production 0.0.0.0:8004 &
	# then, press <Ctrl> + <C>
	```

#### <a name="bb-frontend-and-backend"></a>Frontend and backend

Combine actions from [Frontend](#bb-frontend) and [Backend](#bb-backend), respectively.

### <a name="heroku"></a>ma-admin.herokuapp.com

1. Login to Heroku Container Registry:
    ```bash
    heroku container:login
    ```
    or
    ```bash
    docker login --username= --password=$(heroku auth:token) registry.heroku.com
    ```
    
2. Re/build and push the `web` (aka `django`) image if needed:
    ```bash
    docker build -f ./heroku/web/Dockerfile -t registry.heroku.com/ma-admin/web . && \ 
    docker push registry.heroku.com/ma-admin/web
    ```
    
3. Re/build and push the `node_fillformsirs` (aka `node_fillformsirs`) image if needed:
    ```bash
    docker build -f ./heroku/node_fillformsirs/Dockerfile -t registry.heroku.com/ma-admin/node_fillformsirs . && \ 
    docker push registry.heroku.com/ma-admin/node_fillformsirs
    ```

4. Backup [tranquil.newagelabs.co](#bare-bones) MongoDB data and restore to ma-admin.herokuapp.com if needed:
    1. Backup:
        ```bash
        ssh \
           -i <ubuntu_id_rsa path> \ 
           ubuntu@db.eindocs.com \
           <<'ENDSSH'
               mongodump --gzip --out ./backups/latest
           ENDSSH
        ```
    2. Copy the backup locally:
        ```bash
        rsync \
            --verbose \
            --rsh="ssh -i <ubuntu_id_rsa path>" \
            --archive \
            --checksum \
            --compress \
            --human-readable \
            --stats \
            ubuntu@db.eindocs.com:~/backups/latest \
            ./backups/mongo
        ``` 
    3. Restore from the backup remotely:    
        ```bash
        mongorestore \
            --host=ds121950.mlab.com \
            --port=21950 \
            --db=heroku_ldthbl9q \
            --username=heroku_ldthbl9q \
            --password=raivh1buu3fvad26oomuc3v9ei \
            --gzip \
            ./backups/mongo/latest/ticketingapp    
        ``` 


## <a name="local-docker"></a>Local Docker Setup

### <a name="replicate-mongo"></a>Replicate MongoDB

Refer to [ma-admin.herokuapp.com](#heroku), section 4.1, 4.2 for instructions on backing up legacy MongoDB databse and copying the backup locally, respectively.

Then, 

```bash
docker-compose -f local.yml run --rm mongo mongorestore \
   --host=mongo \
   --db=ticketingapp \
   --gzip \
   /backups/latest/ticketingapp
```

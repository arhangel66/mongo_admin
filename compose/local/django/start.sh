#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset


python manage.py migrate
create_superuser_py=$(cat <<-EOF
    from django.contrib.auth import get_user_model;
    User = get_user_model();
    User.objects.filter(username='${DJANGO_DEFAULT_SUPERUSER_USERNAME}').exists() or \
    User.objects.create_superuser(
        username='${DJANGO_DEFAULT_SUPERUSER_USERNAME}',
        email='${DJANGO_DEFAULT_SUPERUSER_EMAIL}',
        password='${DJANGO_DEFAULT_SUPERUSER_PASSWORD}',
    );
EOF
)
echo ${create_superuser_py} | python manage.py shell
python manage.py runserver_plus 0.0.0.0:8000

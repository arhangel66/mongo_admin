import re
from datetime import timedelta
from logging import getLogger

import pymongo
import pytz
from bson import CodecOptions, ObjectId
from django.conf import settings
from django.utils import timezone

from mongo_admin.services.mongo import get_mongo_client

logger = getLogger(__name__)


class PyMongoHelper(object):
    db = None
    entry = None

    def _connect(self):
        if not self.db:
            self.db = get_mongo_client()[settings.MONGO_APP]
        return self.db

    def _entries(self):
        self._connect()
        entries = self.db.olhapp_entry.with_options(
            codec_options=CodecOptions(tz_aware=True,
                                       tzinfo=pytz.timezone(settings.MONGO_TZ)))
        return entries

    def payments(self):
        self._connect()
        payments = self.db.Payment.with_options(
            codec_options=CodecOptions(tz_aware=True,
                                       tzinfo=pytz.timezone(settings.MONGO_TZ)))
        return payments

    def payments_dict(self):
        d_payments = {}
        for payment in self.payments().find():
            payment['_id'] = str(payment.get('_id', ''))
            if 'documentId' in payment:
                d_payments[str(payment['documentId'])] = payment
        return d_payments

    def get_entry(self, id=None, wrapper=False):
        from mongo_admin.main.utils import EntryWrapper
        if self.entry is None:
            d_payments = self.payments_dict()
            if id is None:
                id = self.kwargs.get('id')
            entry = self._entries().find_one({"_id": ObjectId(id)})
            entry['payment'] = d_payments.get(str(entry['_id']), {})
            entry['id'] = str(entry['_id'])
            if wrapper:
                entry = EntryWrapper(entry)
            self.entry = entry
        return self.entry

    def set_filing_status(self, id, status):
        res = self._entries().update_one({
            '_id': ObjectId(id)
        }, {
            '$set': {
                'orderInfo.filingStatus': status
            }
        }, upsert=False)
        return res

    def append_tag(self, id, tag):
        entry = self.get_entry(id)
        res = self._entries().update_one({
            '_id': ObjectId(id)
        }, {
            '$set': {
                'tags': (entry.get('tags', '') + ',' + tag).strip(',')
            }
        }, upsert=False)

    def get_leads(self, time_from=None, time_to=None, tag=None, filter_orders=True):
        qs = {'orderInfo.paymentStatus': '',
              '$and': [{'tags': {'$not': re.compile('.*test.*', re.IGNORECASE)}}]
              }
        if time_from:
            qs.update({'orderInfo.created': {'$gte': time_from}})
        if time_to:
            qs.setdefault('orderInfo.created', {}).update({'$lt': time_to})
        if tag:
            qs.update({'tags': re.compile('.*%s.*' % tag, re.IGNORECASE)})
        entries = self._entries().find(qs).sort("_id", pymongo.DESCENDING)

        # clean choosen leads, from orders:
        if filter_orders:
            orders = self.get_orders(time_from=time_from)

            email_orders = set(
                [order.get('taxIdRecipientInformation', {}).get('receipientEmailEIN') for order in orders])
            print(len(email_orders))
            leads = []
            for lead in entries:
                if lead.get('taxIdRecipientInformation', {}).get('receipientEmailEIN') not in email_orders:
                    leads.append(lead)
        else:
            leads = list(entries)
        return leads

    def get_orders(self, time_from=None, time_to=None):
        qs = {'orderInfo.paymentStatus': 'Success', '$and': [{'tags': {'$not': re.compile('.*test.*', re.IGNORECASE)}}]}
        if time_from:
            qs.update({'created': {'$gte': time_from}})
        if time_to:
            qs.setdefault('created', {}).update({'$lt': time_to})
        entries = self._entries().find(qs).sort("_id", pymongo.DESCENDING)
        return entries

    def _call(self, sId, data, method):
        res = self._entries().update_one({
            '_id': ObjectId(sId)
        }, {
            method: data
        }, upsert=False)
        return res

    def set(self, sId, field, value):
        return self._call(sId, {field: value}, '$set')

    def unset(self, sId, field):
        return self._call(sId, {field: ''}, '$unset')

    def push(self, sId, field, value):
        return self._call(sId, {field: value}, '$push')

    def add_email_log(self, sId, dSent):
        res = self._entries().update_one({
            '_id': ObjectId(sId)
        }, {
            '$push': {
                'orderInfo.emailSentInfo': dSent,
            }
        }, upsert=False)
        return res

    def add_letter_log(self, sId, dSent):
        good_fields = ['id', 'url', 'thumbnails', 'created', 'error', 'result']
        dSent = dict((k, dSent.get(k, '')) for k in good_fields)
        if dSent.get('thumbnails'):
            dSent['thumbnail'] = dSent['thumbnails'][0]['large']
        res = self._entries().update_one({
            '_id': ObjectId(sId)
        }, {
            '$push': {
                'orderInfo.letterSentInfo': dSent,
            }
        }, upsert=False)
        return res

    def get_entry_by_id(self, id=""):
        """
        Get entry from mongo by id
        :param id:
        :return:
        """
        return self.get_entry(id, wrapper=True)

    def get_entry_by_order_id(self, order_id=""):
        """
        Get entry from mongo by id
        :param id:
        :return:
        """
        from mongo_admin.main.utils import EntryWrapper
        entry = self._entries().find_one({"orderInfo.orderId": order_id})
        # print(169, entry)
        if entry:
            return EntryWrapper(entry)
        return None

    def save_signature(self, sId, signature):
        res = self._entries().update_one({
            '_id': ObjectId(sId)
        }, {
            '$push': {
                'signatureId': signature,
            }
        }, upsert=False)

    def add_fax_log(self, sId, dSent):
        res = self._entries().update_one({
            '_id': ObjectId(sId)
        }, {
            '$push': {
                'orderInfo.faxSentInfo': dSent,
            }
        }, upsert=False)
        return res

    def save_tracking_number(self, sId, trackingNumber):
        res = self._entries().update_one({
            '_id': ObjectId(sId)
        }, {
            '$push': {
                'orderInfo.tracking_number': trackingNumber,
            }
        }, upsert=False)

    def get_any_entries_by_email(self, email):
        qs = {'taxIdRecipientInformation.receipientEmailEIN': re.compile(email, re.IGNORECASE)}
        entries = self._entries().find(qs).sort("_id", pymongo.DESCENDING)
        return self.return_list_wrappers(entries)

    def get_refunded_entries(self):
        entries = self._entries().find({"tags": re.compile('.*Refunded.*', re.IGNORECASE),
                                        # "orderInfo.created": {"$gt": timezone.now() - timedelta(days=3)}
                                        })
        return self.return_list_wrappers(entries)

    def get_paid_entry_by_email(self, email, days_ago=7):
        """ it will return one entry, who paid money in last week.
        :param email: email for serarch paid entry
        :type email: str
        :type days_ago: int,>=0,<1000

        :rtype: EntryWrapper|None
        """
        # has email, made payment in last 7 days, it  has only one order.
        min_date = timezone.now() - timedelta(days=days_ago)
        qs = {'taxIdRecipientInformation.receipientEmailEIN': re.compile(email, re.IGNORECASE),
              "orderInfo.created": {"$gt": min_date},
              "orderInfo.paymentStatus": "Success"}
        logger.debug(qs)
        entries = self._entries().find(qs).sort("_id", pymongo.DESCENDING)
        logger.info(entries.count())
        if entries.count() == 1:
            for entry in entries:
                logger.debug(entry)
                from mongo_admin.main.utils import EntryWrapper
                return EntryWrapper(entry)
        return None

    def save_email(self, sId, data):
        return self.set(sId, 'queuedEmail', data)

    def save_short_code(self, sId, short):
        return self.set(sId, 'short_code', short)

    def queued_email_sent(self, sId):
        return self.set(sId, 'queuedEmail.sent_at', timezone.now())

    def return_list_wrappers(self, entries):
        from mongo_admin.main.utils import EntryWrapper
        list_entries = []
        for entry in entries:
            list_entries.append(EntryWrapper(entry))
        return list_entries

    def get_queued_emails(self):
        """
        return entries with ready for send queued email
        :return:
        """
        qs = {'queuedEmail.send_at': {'$lte': timezone.now()}, "queuedEmail.sent_at": {"$exists": False}}
        entries = self._entries().find(qs)
        return self.return_list_wrappers(entries)

    def get_fresh_entries(self, minutes=5):
        time_prev_5_min = timezone.now() - timedelta(minutes=minutes)
        qs = {'orderInfo.created': {'$gte': time_prev_5_min}, 'orderInfo.paymentStatus': "Success"}
        entries = self._entries().find(qs)
        return self.return_list_wrappers(entries)

    def add_sms_log(self, sId, dSent):
        dSent['created'] = timezone.now()
        res = self._entries().update_one({
            '_id': ObjectId(sId)
        }, {
            '$push': {
                'orderInfo.smsSentInfo': dSent,
            }
        }, upsert=False)
        return res

    def add_refund_log(self, sId, dSent):
        self._entries().update_one({
            '_id': ObjectId(sId)
        }, {
            '$push': {
                'refundLogs': dSent,
            }
        }, upsert=False)

    def save_ein(self, id, ein, legal_name):
        self._entries().update_one({
            '_id': ObjectId(id)
        }, {
            '$set': {
                'orderInfo.irsEin': ein,
                'orderInfo.irsName': legal_name
            }
        }, upsert=False)

    def get_entry_by_order_ids(self, order_ids=[]):
        """
        Get entry from mongo by id
        :param id:
        :return:
        """
        entries = self._entries().find({"orderInfo.orderId": {'$in': order_ids}})
        # print(169, entry)
        # if entry:
        #     return EntryWrapper(entry)
        return self.return_list_wrappers(entries)

    def apply_new_chargeback_orders(self, order_ids):
        new_chargeback_orders = self.return_list_wrappers(self._entries().find({"orderInfo.orderId": {'$in': order_ids},
                                                      "tags":
                                                          {"$not": re.compile(
                                                              '.*%s.*' % "chargeback",
                                                              re.IGNORECASE)}
                                                      }))

        for entry in new_chargeback_orders:
            # self.append_tag(entry.id, 'chargeback')
            self.set(entry.id, 'is_chargeback', '1')
        return new_chargeback_orders

    def get_chargebacked_entries(self):
        return self.return_list_wrappers(self._entries().find({"is_chargeback": '1'}))

    def set_user(self, id, username):
        entry = self.get_entry(id, wrapper=True)
        if not 'Filed' in  entry.get_param('orderInfo.notes'):
            self._entries().update_one({
                '_id': ObjectId(id)
            }, {
                '$set': {
                    'orderInfo.notes': "Filed by {}".format(username),
                    'additionalInformation.username': username
                }
            }, upsert=False)

    def set_error_message(self, id, error_messages_legacy):
        entry = self.get_entry(id, wrapper=True)
        notes = entry.get_param('orderInfo.notes')
        if not error_messages_legacy in notes:
            if notes:
                notes += ' |\n'
            self.set(id, 'orderInfo.notes', "{}{}".format(notes, error_messages_legacy))



from crispy_forms.bootstrap import StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Div
from django.utils.translation import ugettext as _


class CryspyMixin(object):
    def __init__(self, *args, **kwargs):
        self.button_name = kwargs.pop('button_name', _('Submit'))
        self.button_css_id = kwargs.pop('button_id', None)

        super(CryspyMixin, self).__init__(*args, **kwargs)

        self.helper = FormHelper()
        field_names = [field_name for field_name in self.fields]
        layout = field_names

        if self.button_name != '':
            button = StrictButton(self.button_name, css_class='btn-info', type='submit')
            layout += [button]

        self.helper.layout = Layout(
            *layout
        )
        self.helper.form_method = 'get'


class CryspyInlineMixin(CryspyMixin):
    def __init__(self, *args, **kwargs):
        super(CryspyInlineMixin, self).__init__(*args, **kwargs)
        self.helper.form_class = 'form-inline'
        self.helper.field_template = 'bootstrap3/layout/inline_field.html'  # TODO: this one is simply missing


class CryspyHorizontalMixin(CryspyMixin):
    def __init__(self, *args, **kwargs):
        super(CryspyHorizontalMixin, self).__init__(*args, **kwargs)
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3 col-form-label'
        self.helper.field_class = 'col-sm-9'
        button_kwargs = {}
        if self.button_css_id is not None:
            button_kwargs['css_id'] = self.button_css_id
        button = Div(Div(
            StrictButton(
                self.button_name,
                type='submit',
                css_class='btn-info',
                **button_kwargs
            ),
            css_class='offset-sm-3 col-md-offset-3 col-lg-9 col-md-9',
        ), css_class='form-group')
        layout = self.helper.layout[:-1] + [button]
        self.helper.layout = Layout(*layout)

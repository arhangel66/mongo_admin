import datetime
import json
from urllib.parse import parse_qs, urlencode

import requests
from django.conf import settings


class LimeLight(object):

    def call(self, params, method='membership'):
        url = settings.LL.get('url').format(method=method)

        params.update({
            'username': settings.LL.get('username'),
            'password': settings.LL.get('password'),
            "campaignId": settings.LL.get('campaign')
        })

        res_text = requests.post(url, params).text
        # if params.get('method') in ['order_find']:
        #     res_text = requests.post(url, params).text
        # else:
        #     res_text = "response_code=100"

        # res = requests.post(url, params)
        if '200' in res_text or 'response_code=100' in res_text:
            return res_text
        return False

    def do_refund(self, sum_refund, order_id, fake=False):
        if fake:
            return True
        params = {
            'method': 'order_refund',
            'order_id': order_id,
            'amount': sum_refund,
        }
        return self.call(params)

    def get_orders(self, email, main_order=False, criteria=None):
        data = {"method": 'order_find',
                'criteria': urlencode({'email': email}),#.replace('%40', '@'),
                'start_date': '06/08/2016',  # datetime.datetime.now().strftime("%m/%d/%Y"), #'06/08/2016',
                'end_date': '06/07/2025',
                'search_type': 'all',
                'campaign_id': '319',
                'return_type': 'order_view',
                # 'criteria': 'chargeback',
                }
        if criteria:
            data.update({'criteria': criteria})
        resp = self.call(data)
        lOrders = []
        dt_main_date = None
        d_order_statuses = {
            '2': 'Approved',
            '6': 'Void/Refund',
            '7': 'Declined',
            '8': 'Shipped',
            '11': 'Pending'
        }
        if 'data' in parse_qs(resp):
            result = parse_qs(resp)['data']
            lResOrders = json.loads(result[0]).values()
            for val in lResOrders:
                order_date = datetime.datetime.strptime(val['acquisition_date'], "%Y-%m-%d %H:%M:%S")
                # if not main_order or (dt_main_date and order_date - dt_main_date <= datetime.timedelta(seconds=2)):
                if (main_order and ((str(main_order) == val['order_id']) or str(main_order) == val['parent_id'])) or not main_order:
                    order_data = {
                        'id': val['order_id'],
                        'date': order_date,
                        'name': val['products[0][name]'],
                        'sum': float(val['order_total']),
                        'refund_amount': val['refund_amount'],
                        'is_refund': val['is_refund'],
                        'is_void': val['is_void'],
                        'is_chargeback': val.get('is_chargeback', 'no'),
                        'status': d_order_statuses.get(val['order_status']),
                        'email': val['email_address']
                    }
                    lOrders.append(order_data)
        return lOrders

    def discount_order(self, order_id, price=None):
        data = {
            'method': 'NewOrderCardOnFile',
            'billingSameAsShipping': 'YES',
            'productId': 513,
            'shippingId': 17,
            'previousOrderId': order_id,
            'preserve_force_gateway': 1,
            'dynamic_product_price_513': price,
        }
        resp = self.call(data, 'transact')
        return resp

    def get_list_chargebacked_orders(self):
        data = {"method": 'order_find',
                'start_date': '06/08/2016',  # datetime.datetime.now().strftime("%m/%d/%Y"), #'06/08/2016',
                'end_date': '06/07/2025',
                'search_type': 'all',
                'campaign_id': '319',
                # 'return_type': 'order_view',
                'criteria': 'chargeback',
                }
        resp = self.call(data)
        order_ids = parse_qs(resp)['order_ids'][0].split(',')
        return order_ids

import json
from typing import Dict, Any

from bson import json_util


def convert_mongo_bson_to_relaxed_not_extended_json_dict(
    entry: Dict[str, Any]
) -> Dict[str, Any]:
    entry_copy = entry.copy()
    if '_id' in entry_copy:
        entry_copy['_id'] = str(entry_copy['_id'])

    relaxed_not_extended_entry = {}

    relaxed_extended_entry = json.loads(
        json_util.dumps(
            entry_copy,
            json_options=json_util.JSONOptions(
                datetime_representation=json_util.DatetimeRepresentation.ISO8601,
                json_mode=json_util.JSONMode.RELAXED
            )
        )
    )
    for key, value in relaxed_extended_entry.items():
        if isinstance(value, dict):
            if len(value) == 1:
                the_key = list(value.keys())[0]
                if the_key.startswith('$'):
                    value = value[the_key]
            else:
                value = convert_mongo_bson_to_relaxed_not_extended_json_dict(value)
        relaxed_not_extended_entry[key] = value

    return relaxed_not_extended_entry

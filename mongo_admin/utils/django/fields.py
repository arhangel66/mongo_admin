from os.path import splitext, join
from typing import Type
from uuid import uuid4

from django.db.models import Model


def generate_upload_to(
    base_path: str,
    instance: Type[Model],
    filename: str,
):
    filename_extension = splitext(filename)[1]
    unique_filename = "{}{}".format(uuid4(), filename_extension)
    return join(base_path, unique_filename)

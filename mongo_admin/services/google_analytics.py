import requests


def refund_analytics_transaction(sum, product_sku, order_id, client_id=None):
    TRACKING_URI = 'https://ssl.google-analytics.com/collect'
    import uuid

    # return 200

    if not client_id:
        client_id = uuid.uuid4()

    refund = {
        "v": "1",  # Version.  # Version.
        "tid": "UA-123733714-1",  # Tracking ID / Property ID.
        "cid": client_id,  # Anonymous Client ID.
        "t": "event",  # Event hit type.
        "ec": "Ecommerce",  # Event Category. Required.
        "ea": "Refund",  # Event Action. Required.
        "ni": "1",  # Non-interaction parameter.
        "ti": order_id,  # Transaction ID. Required.
        "pa": "refund",
        "pr1id": product_sku,
        "pr1qt": "1",
        "pr1pr": sum,
        "dh": "tax-filing-forms.com",
    }

    res = requests.post(TRACKING_URI, data=refund)
    if res.status_code == 200:
        return True
    return False
    # print(res)



# 1183774343.1532437074

import io
import traceback
import zipfile
from .binder import bind_api_method


class HelloFaxAPI(object):

    def __init__(self, host='www.hellofax.com',
                 api_root='/apiapp.php/v1',
                 email=None,
                 password=None,
                 guid=None):
        self.host = host
        self.api_root = api_root
        self.email = email
        self.password = password
        self.guid = guid


    #Allows send a fax
    send_fax = bind_api_method(
        path = "/Accounts/{guid}/Transmissions",
        method = "POST",
        allowed_param = ["To", 'file']
    )

    #get account details
    account_details = bind_api_method(
        path = "/Accounts/{guid}",
        method = "GET",
    )

    #update account details
    update_account_details = bind_api_method(
        path = "/Accounts/{guid}",
        method = "POST",
        allowed_param = [] #need to access official API documentation
    )

    #get transmission of account
    transmissions = bind_api_method(
        path = "/Accounts/{guid}/Transmissions",
        method = "GET",
    )

    fax_lines = bind_api_method(
        path = "/Accounts/{guid}/FaxLines",
        method = "GET"
    )

    find_fax_numbers = bind_api_method(
        path = "/AreaCodes",
        method= "GET",
        allowed_param= ["statecode"]
    )

    transmission = bind_api_method(
        path = "/Accounts/{guid}/Transmissions/{transmission}",
        method= "GET",
        allowed_param= ["transmission"]
    )


import requests

HELLOSIGN_API_KEY = 'da469e89bc17605274f4553244737d9ad71ebb99ff4849ef9436ac423c751e1e'
signature_request_id = 'dcf9a8126ddda887534c4086fad7b18d8faa86ef'


def get_signature_request_file(signature_request_id, *args, **kwargs):
    path = 'https://api.hellosign.com/v3/signature_request/files/' + signature_request_id
    r = requests.get(path, params=kwargs, auth=(HELLOSIGN_API_KEY, ''))
    try:
        zip = zipfile.ZipFile(file=io.BytesIO(r.content))
        pdf = zip.open(zip.namelist()[0])
        return pdf
    except:
        traceback.print_exc()
        return None

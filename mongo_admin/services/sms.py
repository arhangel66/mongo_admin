from django.conf import settings
from django.template.loader import render_to_string


def send_sms(msg, to):
    from twilio.rest import Client

    account_sid = settings.SMS_ACCOUNT_ID
    auth_token = settings.SMS_AUTH_TOKEN
    client = Client(account_sid, auth_token)

    if settings.SMS_REAL_SENDING == 'True':
        try:
            message = client.messages.create(
                from_='+18606064652',
                body=msg,
                to=to
            )
        except:
            return False
        if message.sid:
            return {
                'body': msg,
                'to': to,
                'message_id': message.sid,
            }
        else:
            return False
    else:
        return {
            'body': msg,
            'to': to,
            'message_id': "Fake sending for check body and to",
        }



def send_ein_sms(entry):
    msg = render_to_string('sms/ein_ready.html', {'entry': entry})
    to = entry.phone_sms_format
    return send_sms(msg, to)

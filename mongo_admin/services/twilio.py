from twilio.rest import Client

_client = Client()


def get_twilio_client() -> Client:
    return _client

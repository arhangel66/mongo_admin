from datetime import date
from logging import getLogger

logger = getLogger(__name__)
from logging import getLogger

logger = getLogger(__name__)
from django.conf import settings
from django.template.loader import render_to_string
from slugify import slugify
from weasyprint import CSS, HTML


def get_ein_pdf_filename(entry):
    filename = slugify("{IRS_NAME}-{IRS_EIN}".format(**{
        'IRS_NAME': entry.get_param('orderInfo.irsName'),
        'IRS_EIN': entry.get_param('orderInfo.irsEin')
    })) + '.pdf'
    return filename


def get_order_confirmation_pdf(entry):
    template = "pdf/order_confirmation.html"
    return _template_to_pdf(template, entry, {'today': date.today()})


def get_ein_pdf_content(entry):
    template = "pdf/entry_oficial.html"
    return _template_to_pdf(template, entry)


def _template_to_pdf(template, entry, params=None):
    if params is None:
        params = {}
    style = CSS(string="""
            @page {font-family: Arial, Verdana, Helvetica, sans-serif;}
            h1 { font-family: Gentium }
            """)
    context = {'e': entry, 'constants': entry.constants}
    context.update(params)
    full_html = render_to_string(template, context)
    base_url = settings.SITE_URL
    pdf_doc = HTML(string=full_html, base_url=base_url).render(stylesheets=[style])
    pdf_content = pdf_doc.write_pdf()
    return pdf_content

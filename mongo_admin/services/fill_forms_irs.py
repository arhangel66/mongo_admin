from logging import getLogger
from typing import Dict, Any, Optional
from urllib.parse import urljoin, urlencode

import requests
from django.conf import settings
from rest_framework.status import HTTP_200_OK

from mongo_admin.utils.mongo import convert_mongo_bson_to_relaxed_not_extended_json_dict

logger = getLogger(__name__)


class FillFormsIRSAPI:

    @classmethod
    def validate_ssn(
        cls,
        first_name: str,
        last_name: str,
        ssn3: str,
        ssn2: str,
        ssn4: str,
        middle_name: Optional[str] = None
    ) -> Dict[str, Any]:
        url = cls._construct_url("validate-ssn")
        logger.debug(f"FillFormsIRSAPI.verify_user_data: url = {url}")
        params = {
            'firstName': first_name,
            'lastName': last_name,
            'applicantSSN3': ssn3,
            'applicantSSN2': ssn2,
            'applicantSSN4': ssn4,
        }
        if middle_name is not None:
            params['middleName'] = middle_name
        logger.debug(f"FillFormsIRSAPI.validate_ssn: {params}")

        response = requests.get(url, params=params)
        logger.debug(f"FillFormsIRSAPI.validate_ssn: {response}")

        if response.status_code != HTTP_200_OK:
            logger.warning(f"FillFormsIRSAPI.validate_ssn: response.status_code = {response.status_code}")

        return response.json()

    @classmethod
    def verify_user_data(
        cls,
        entry: Dict[str, Any],
    ) -> Dict[str, Any]:
        url = cls._construct_url("verify-user-data", format="json")
        logger.debug(f"FillFormsIRSAPI.verify_user_data: url = {url}")

        data = convert_mongo_bson_to_relaxed_not_extended_json_dict(entry)
        response = requests.post(url, json=data)
        logger.debug(f"FillFormsIRSAPI.verify_user_data: response = {response}")

        if response.status_code != HTTP_200_OK:
            logger.warning(f"FillFormsIRSAPI.verify_user_data: response.status_code = {response.status_code}")

        return response.json()

    @classmethod
    def receive_ein(
        cls,
        entry: Dict[str, Any],
    ) -> Dict[str, Any]:
        url = cls._construct_url("receive-ein", format="json")
        logger.debug(f"FillFormsIRSAPI.receive_ein: url = {url}")

        data = convert_mongo_bson_to_relaxed_not_extended_json_dict(entry)
        response = requests.post(url, json=data)
        logger.debug(f"FillFormsIRSAPI.receive_ein: response = {response}")

        if response.status_code != HTTP_200_OK:
            logger.warning(f"FillFormsIRSAPI.receive_ein: response.status_code = {response.status_code}")

        return response.json()

    @classmethod
    def _get_base_url(cls) -> str:
        return f"{settings.FILLFORMSIRS_SCHEMA}://{settings.FILLFORMSIRS_HOST}:{settings.FILLFORMSIRS_PORT}"

    @classmethod
    def _construct_url(
        cls,
        endpoint_url: str,
        **params,
    ) -> str:
        return urljoin(cls._get_base_url(), f"{endpoint_url}?{urlencode(params)}")

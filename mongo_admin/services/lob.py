from logging import getLogger

import lob
from django.conf import settings
from django.template.loader import render_to_string
from django.utils import timezone

logger = getLogger(__name__)

def send_letter(entry):
    letter_template = 'letters/ein_ready.html'
    lob.api_key = settings.LOB_API
    letter_html = render_to_string(letter_template, context={'e': entry.d, 'constants': entry.constants})

    try:
        from_address = lob.Address.create(
            name='Specialist',
            company='Tax Filing Forms',
            address_line1='16192 Coastal Hwy',
            address_line2='',
            address_city='Lewes',
            address_state='DE',
            address_zip='19958'
        )

        result = dict(lob.Letter.create(
            description='letter description',
            # metadatxa={
            #     'campaign': 'billing_statements',
            #     'csv': input_filename
            # },
            to_address={
                'name': "%s %s" % (entry.first_name, entry.last_name),
                'address_line1': entry.get_param('businessInformation.address', default='111'),
                # TODO need to clarify, maybe we need payment address
                'address_line2': '',
                'address_city': entry.get_param('businessInformation.city', default='111'),
                'address_zip': entry.get_param('businessInformation.zip', default='111'),
                'address_state': entry.get_param('businessInformation.state', default='CA')
            },
            from_address=from_address.id,
            file=letter_html,
            merge_variables={'name': 'bob'},
            extra_service='',  # certified if we need tracking number
            color=False
        ))
        result['result'] = True
    except Exception as e:
        result = {'result': False, 'error': str(e)}
    result['created'] = timezone.now()
    return result


def get_details(id):
    try:
        lob.api_key = settings.LOB_API
        params = lob.Letter.retrieve(id)
    except:
        logger.error('Cant get letter with id %s' % id)
        params = {}
    return params

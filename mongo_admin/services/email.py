from logging import getLogger

from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from mongo_admin.services import pdf

logger = getLogger(__name__)



def send_email_to_lead(entry):
    """
    DocumentWrapper object
    :param entry: send email and return True or False
    :return:
    """
    return send_template_email(entry, "emails/lead_email.html", 'You forgot to finish your application...')


def send_template_email(entry, template_name, subject):
    html_content = render_to_string(template_name, context={"e": entry})
    text_content = strip_tags(html_content)

    email = EmailMultiAlternatives(
        subject,
        text_content,
        entry.domain_config.SUPPORT_EMAIL,
        [entry.email],
        # ['arhangel662@gmail.com'],
        bcc=entry.domain_config.EMAIL_BCC
    )
    if html_content != text_content:
        email.attach_alternative(html_content, "text/html")
    res = email.send()
    return res


def send_confirmation_pdf_email(entry):
    email = EmailMultiAlternatives(
        "Order confirmation",
        " ",
        entry.domain_config.SUPPORT_EMAIL,
        [entry.email],
        # ['arhangel662@gmail.com'],
        bcc=entry.domain_config.EMAIL_BCC
    )
    pdf_content = pdf.get_order_confirmation_pdf(entry)
    email.attach('order_confirmation.pdf', pdf_content, "application/pdf")
    return email.send()


def send_refund_email(entry):
    logger.debug('sending refund email')
    return send_template_email(entry, 'emails/refund.html', "We have refunded your order")


"""
Московский институт психоанализа

Вебинарный класс
2-3 раза в неделю встречи с преподавателем

Домашние задания
профпереподготовка 2 года
Психологическое консультирование и диагностика личности
практикоориентированная
преподаватель консультирует


с 19 до 20:30
3 раза в неделю

За 55тр


"""

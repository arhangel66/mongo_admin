import re
from logging import getLogger
from urllib.parse import urlencode

from django.conf import settings
from django.template.loader import render_to_string
from zenpy import Zenpy
from zenpy.lib.api_objects import Comment

from mongo_admin.main.utils import domain_config

logger = getLogger(__name__)



def get_zendesk_client(zendesk_subdomain) -> Zenpy:
    domain_converter = {
        'eindocshelp': 'eindocs'
    }
    domain_key = domain_converter.get(zendesk_subdomain, zendesk_subdomain)
    config = domain_config(domain_key)
    return Zenpy(**{
        'email': config.ZENDESK_EMAIL,
        'token': config.ZENDESK_TOKEN,
        'subdomain': config.ZENDESK_SUBDOMAIN
    })


def get_tickets(zendesk_subdomain):
    # Default
    zenpy_client = get_zendesk_client(zendesk_subdomain)

    tickets = zenpy_client.search(type='ticket', status_less_than='pending', minus='tags:commented')
    return tickets


def get_attachment_tickets(zendesk_subdomain):
    # Default
    zenpy_client = get_zendesk_client(zendesk_subdomain)

    tickets = zenpy_client.search(type='ticket', status_less_than='pending', minus='tags:commented', has_attachment=True)
    return tickets

def filter_tickets(tickets):
    search_words = settings.ZENDESK_SEARCH_WORDS
    filtered_tickets = []
    for ticket in tickets:
        # ignore commented
        if 'commented' in ticket.tags:
            continue

        # ignore stripe
        if ticket.requester.email == 'support@stripe.com':
            continue

        for key in search_words:
            if key.lower() in "%s %s" % (ticket.subject.lower(), ticket.description.lower()):
                logger.debug("'%s' found in '%s'" % (key.lower(), ticket.subject.lower()))
                if ticket not in filtered_tickets:
                    filtered_tickets.append(ticket)
                break
    return filtered_tickets


def add_comment_with_links(ticket, entries, zendesk_subdomain):
    client = get_zendesk_client(zendesk_subdomain)
    params = {
        "payment_status": "Success",
        "universal": ticket.email,
        "show_test": 'on',
    }
    url = "https://tranquil.newagelabs.co/list/?%s" % urlencode(params)
    comment = render_to_string("zendesk/comment.html", {'url': url, 'entries': entries})
    ticket.comment = Comment(body=comment, public=False)
    client.tickets.update(ticket)

    logger.debug('prepare_comment:\n' + comment)

    # add 'commented' tag to ticket
    tags = client.tickets.add_tags(id=ticket.id, tags=['commented'])
    logger.debug('add "commented" tag: %s' % tags)
    return True


def get_ticket_email(ticket):
    """
    :type ticket:Ticket
    :rtype: str
    :return email as string or None
    """
    if ticket.requester.email != 'info@tax-filing-forms.com':
        email = ticket.requester.email
    else:
        email = re.search(r'[\w\.-]+@[\w\.-]+', ticket.description).group(0)
    return email


def set_refunded(ticket, zendesk_subdomain, amount):
    client = get_zendesk_client(zendesk_subdomain)

    # add comment
    refunded_comment = "Refunded %s$" % amount
    ticket.comment = Comment(body=refunded_comment, public=False)
    client.tickets.update(ticket)
    logger.debug('added comment: %s' % refunded_comment)

    # set refunded tag
    tags = client.tickets.add_tags(id=ticket.id, tags=['Refunded'])
    logger.debug('add "commented" tag: %s' % tags)

    # close ticket
    ticket.status = "solved"
    client.tickets.update(ticket)
    logger.debug('set ticket status as "Solved"')
    return True


def get_attachments(tickets):
    """it will attach to each ticket attachments = ['id', 'filename']"""
    tickets.attachments = []
    return tickets


def filter_incorporate_tickets(tickets):
    """
    it will return only tickets with 'incorporate' word in body/subject
    :param tickets:
    :return:
    """
    return tickets


def save_attachments(ticket, entry):
    """will save attacments to disk and add info about it to entry"""
    return None


def save_attachment(ticket, entry):
    """will save attacments to disk
    return path
    """
    return None

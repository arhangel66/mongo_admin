import json

import requests
from django.template.loader import render_to_string
from django.conf import settings

webhook_url = "https://hooks.slack.com/services/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"


def html_decode(s):
    """
    Returns the ASCII decoded version of the given HTML string. This does
    NOT remove normal HTML tags like <p>.
    """
    htmlCodes = (
        ("'", '&#39;'),
        ('"', '&quot;'),
        ('>', '&gt;'),
        ('<', '&lt;'),
        ('&', '&amp;')
    )
    for code in htmlCodes:
        s = s.replace(code[1], code[0])
    return s


def post_message(text, channel="temp"):
    slack_data = {'text': text, 'channel': channel, 'username': 'eindocs-python-bot'}
    response = requests.post(
        webhook_url, data=json.dumps(slack_data),
        headers={'Content-Type': 'application/json'}
    )
    if response.status_code != 200:
        raise ValueError(
            'Request to slack returned an error %s, the response is:\n%s'
            % (response.status_code, response.text)
        )


def send_message_with_links(ticket, entries, zendesk_subdomain, refunded_amount):
    if len(list(entries)):
        entry = entries[0]
    else:
        entry = None

    context = {
        'ticket': ticket,
        'description': ticket.description.split('\n')[0] if '\n' in ticket.description else ticket.description,
        'entry': entry,
        'zendesk_subdomain': zendesk_subdomain,
        'refunded_amount': refunded_amount
    }

    message = render_to_string('zendesk/slack_message.html', context=context)
    for channel in settings.ZENDESK_SLACK_CHANNELS:
        post_message(html_decode(message), channel=channel)

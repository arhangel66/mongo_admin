from django.conf import settings
from pymongo import MongoClient
_client = MongoClient(host=settings.MONGO_HOST, port=settings.MONGO_PORT)


def get_mongo_client() -> MongoClient:
    return _client

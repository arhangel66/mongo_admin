(function() {
  'use strict';
  var tags = {
    options: [
      {text: 'done', className: 'success'},
      {text: 'error', className: 'danger'},
      {text: 'warning', className: 'warning'},
      {text: 'info', className: 'info'},
      {text: 'test', className: 'secondary'},
    ],
    formatResult: function(state, container) {
      var exp = new RegExp(state.id, 'i');
      var opt = this.options.find(function(el) {
        return exp.test(el.text);
      });
      var className = (opt && opt.className) || 'primary';
      var $state = $(`<span class="badge badge-${className}">${state.text}</span>`);
      return $state;
    },
    formatSelection: function(state, container) {
      var exp = new RegExp(state.id, 'i');
      this.options.forEach(function(el) {
        if (exp.test(el.text)) {
          $(container).toggleClass(el.className, state.selected);
        }
      });
      return state.id;
    },
    updateOnServer: function(entryId, tags) {
      $.get(`/leads/${entryId}/`, { tags });
    },
    changeListner: function(e) {
      var entryId = e.target.dataset.entryId;
      var options = e.target.options;
      var tags = (
        Array.from(options)
             .filter(opt => opt.selected)
             .map(opt => opt.value)
             .join(',')
      );
      this.updateOnServer(entryId, tags)
    },
    init: function() {
      var selector = '.js-tags';
      $(selector).select2({
        tags: true,
        templateResult: this.formatResult.bind(this),
        templateSelection: this.formatSelection.bind(this),
        containerCssClass: 'tags',
      });
      $(selector).on('change.select2', this.changeListner.bind(this));
    },
  };
  tags.init();
}());

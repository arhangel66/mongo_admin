from django.apps import AppConfig


class USPSTrackingConfig(AppConfig):
    name = "mongo_admin.usps_tracking"
    verbose_name = "USPSTracking"

    def ready(self):
        try:
            # noinspection PyUnresolvedReferences
            from . import signals  # noqa F401
        except ImportError:
            pass

from django.conf.urls import url

from mongo_admin.usps_tracking.views import home

urlpatterns = [url(regex=r"^$", view=home, name="home")]

from django.http import HttpResponseNotAllowed, HttpResponseBadRequest, HttpResponse

from mongo_admin.usps_tracking.commands import customize_tracking_page
from mongo_admin.usps_tracking.forms import TrackForm


def track_view(request):
    # TODO: to DRF view
    if request.method == "GET":
        form = TrackForm(request.GET)
        if form.is_valid():
            tracking_number = form.cleaned_data.get("tracking_number")
            line1 = form.cleaned_data.get("line1")
            line2 = form.cleaned_data.get("line2")

            customized_tracking_page = customize_tracking_page(
                tracking_number, line1, line2
            )

            http_response = HttpResponse(
                content=customized_tracking_page,
                content_type="text/html; charset=utf-8",
            )
            http_response["Content-Length"] = len(customized_tracking_page)
            return http_response
        return HttpResponseBadRequest()
    return HttpResponseNotAllowed(["GET"])


home = track_view

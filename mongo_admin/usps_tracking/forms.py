from django.forms import Form, CharField


class TrackForm(Form):
    # TODO: to DRF serializer
    tracking_number = CharField()
    line1 = CharField()
    line2 = CharField()

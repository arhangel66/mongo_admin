from typing import Optional

from bs4 import BeautifulSoup
from requests import Session

from mongo_admin.usps_tracking.imgs import (
    img_icon_tooltip,
    img_black_icon_carat_down,
    img_delivered,
    img_icon_carat_up,
    img_icon_cancel,
    img_orange_icon_carat_down,
)


def customize_tracking_page(
    tracking_number: str, line1: str, line2: str, session: Optional[Session] = None
) -> bytes:
    if session is None:
        session = Session()
        session.headers[
            "User-Agent"
        ] = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.131 Safari/537.36"  # https://stackoverflow.com/a/23652207

    base_url = "https://tools.usps.com"
    url = f"{base_url}/go/TrackConfirmAction?tLabels={tracking_number}"

    response = session.get(url)

    markup_with_absolute_links = (
        response.text.replace('="/', f'="{base_url}/')
        .replace(
            "/global-elements/navigation/images/blank.gif",
            f"{base_url}/global-elements/navigation/images/blank.gif",
        )
        .replace('"images/', f'"{base_url}/go/images/')
        .replace(
            "url(images/Tracking_DSKTP_Promo_Ad2_865x410.jpg)",
            f"url({base_url}/go/images/Tracking_DSKTP_Promo_Ad2_865x410.jpg)",
        )
        .replace("url(/global-elements/", f"url({base_url}/global-elements/")
        .replace('src="/global-elements/', f'src="{base_url}/global-elements/')
        .replace(r'src\="\/global-elements\/', fr'src\="{base_url}\/global-elements\/')
        .replace('<i class="icon-tooltip"></i>', img_icon_tooltip)
        .replace(
            'Get Updates <i class="icon-carat_down"></i>',
            f"Get Updates {img_orange_icon_carat_down}",
        )
        .replace('<i class="icon-carat_down"></i>', img_black_icon_carat_down)
        .replace('<span class="icon-carat_down"></span>', img_black_icon_carat_down)
        .replace("<strong>Delivered</strong>", img_delivered)
        .replace('<span class="icon-carat_up"></span>', img_icon_carat_up)
        .replace('<i class="icon-carat_up"></i>', img_icon_carat_up)
        .replace('<i class="icon-cancel"></i>', img_icon_cancel)
        .replace(
            '<span class="icon-cancel" aria-hidden="true"></span>', img_icon_cancel
        )
    )

    bs = BeautifulSoup(markup=markup_with_absolute_links, features="lxml")

    line1_selector = bs.select_one("div.status_feed p:nth-of-type(2)")
    line1_selector.string = line1

    line2_selector = bs.select_one("div.status_feed p:nth-of-type(3)")
    line2_selector.string = line2

    return bs.encode_contents()

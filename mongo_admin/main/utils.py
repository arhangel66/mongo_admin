import calendar
import datetime
import json
import traceback
from importlib import import_module
from logging import getLogger

from django.core.files.storage import default_storage

from mongo_admin.users.models import User

logger = getLogger(__name__)
from bson import ObjectId
from bson.son import SON
from django.conf import settings
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils import timezone
from slugify import slugify
from weasyprint import CSS, HTML
from weasyprint.formatting_structure.boxes import TableBox

from mongo_admin.core.db import PyMongoHelper
from mongo_admin.limelight.api import LimeLight
from mongo_admin.services import lob, sms, google_analytics
from mongo_admin.services.pyhellofax.api import get_signature_request_file
from mongo_admin.services.pyhellofax.exceptions import HelloFaxAPIException


def get_first_word(line):
    import re
    line = line.replace('_', ' ')
    words = re.findall(r"[\w']+", line)
    if words:
        return words[0].lower()
    return ''


def generate_pdf(template, file_object=None, response=None, context=None, minify=False, request=None):
    """
    Generate PDF content from given template and context.
    - If the file_object is provided, generated content will be write to that
      file then return it.
    - If the response is provided, it will be returned with generated content
      inside.
    - Otherwise, the PDF as bytestring will be returned.
    """
    style = CSS(string="""
    @page {font-family: Arial, Verdana, Helvetica, sans-serif;}
    h1 { font-family: Gentium }
    """)

    try:
        # write_obj() return None if target is an file object. Then we return
        # the file_object itself.

        full_html = render_to_string(template, context)
        if request is None:
            base_url = settings.SITE_URL
        else:
            base_url = request.build_absolute_uri()
        pdf_doc = HTML(string=full_html, base_url=base_url).render(stylesheets=[style])

        # Auto resize the page to adapt large content
        for page in pdf_doc.pages:
            for children in page._page_box.descendants():
                if isinstance(children, TableBox):
                    needed_width = children.width + \
                                   page._page_box.margin_left + \
                                   page._page_box.margin_right

                    # Override width only if the table doesn't fit
                    if page.width < needed_width:
                        page._page_box.width = needed_width
                        page.width = needed_width
        pdf_file = pdf_doc.write_pdf(target=file_object)
    except:
        logger.exception(
            _('Error when exporting PDF content using template {}.').format(
                template)
        )

    if file_object is not None:
        return file_object

    # Add pdf content into response object if provided
    if response is not None:
        response.content = pdf_file
        return response

    return pdf_file


import re


def phone_format(n):
    clean_phone_number = re.sub('[^0-9]+', '', n)
    if len(clean_phone_number) > 3:
        formatted_phone_number = re.sub("(\d)(?=(\d{3})+(?!\d))", r"\1-", "%d" % int(clean_phone_number[:-1])) + \
                                 clean_phone_number[-1]
        return formatted_phone_number
    return ''


def sms_format(n):
    clean_phone_number = re.sub('[^0-9]+', '', phone_format(n))
    return "+1{}".format(clean_phone_number)

    # print(clean_phone_number)
    # return format(int(clean_phone_number[:-1]), ",").replace(",", "-") + n[-1]


# TODO: add caching
def get_exist_tags():
    """Return all exist tags with the number of times were used.

    Return example:
    [{'_id': 'SS4', 'count': 5},
     {'_id': 'Refunded', 'count': 2},
     {'_id': 'Done', 'count': 1}]
    """
    pipeline = [
        {'$match': {'tags': {'$exists': True, '$ne': ''}}},  # ignore empty `tags` field  # noqa
        {'$project': {'tags': {'$split': ['$tags', ',']}}},
        {'$unwind': '$tags'},
        {'$group': {'_id': '$tags', 'count': {'$sum': 1}}},
        {'$sort': SON([('count', -1), ('_id', -1)])}
    ]

    # TODO make it case insensitive
    tags = list(PyMongoHelper()._entries().aggregate(pipeline))
    return tags


def domain_config(domain_key):
    try:
        config = import_module("mongo_admin.domains.{}".format(domain_key))
    except:
        config = import_module("mongo_admin.domains.tax-filing-forms")
    return config


class EntryWrapper(object):
    def __init__(self, params):
        if params is None:
            params = {}
        self.d = params
        self.d['id'] = self.get_param('_id')
        self.id = self.get_param('_id')
        self.helper = None
        self.orders = None

    @property
    def domain_key(self):
        return self.get_param('domain_key', default='tax-filing-forms')

    @property
    def domain_config(self):
        return domain_config(self.domain_key)

    @property
    def constants(self):
        return self.domain_config.CONSTANTS

    def get_email_connection(self):
        connection_settings = {
            'backend': 'anymail.backends.mailgun.EmailBackend',
            'api_key': self.domain_config.ANYMAIL.get('MAILGUN_API_KEY')
        }
        print(connection_settings)
        # return get_connection(connection_settings)

    def get_helper(self):
        if self.helper is None:
            self.helper = PyMongoHelper()
        return self.helper

    def get_storage_path(self, file):
        path = "{}/{}".format(self.id, file.name)
        return path

    def save_file(self, name, file):
        """

        :param name: like 'tag', 'ein' or 'articles_of_incorporation'
        :param file: file object.
        :return:
        """
        if not self.has_file(name):
            path = self.get_storage_path(file)
            with default_storage.open(self.get_storage_path(file), 'w') as f_storage:
                f_storage.write(file.read())
                self.save_filename_to_mongo(name, file.name, path)
            return True
        return False

    def save_filename_to_mongo(self, name, filename, path):
        dFile = {
            'name': name,
            'filename': filename,
            'path': path,
            'updated': timezone.now(),
            'url': "{}{}".format(settings.MEDIA_URL, path)
        }
        self.get_helper().set(self.id, f'files.{name}', dFile)

    def has_file(self, name):
        if self.get_param(f"files.{name}", default=None):
            return True
        return False

    def get_file_dict(self, name):
        d_file = self.get_param(f'files.{name}', default=None)
        if d_file:
            d_file['url'] = "{}{}".format(settings.MEDIA_URL, d_file['path'])
        return d_file

    def get_file_obj(self, name):
        path = self.get_file_dict(name).get('path')
        if path:
            file = default_storage.open(path, 'r')
            return file
        return None

    def del_file(self, name):
        path = self.get_file_dict(name).get('path')
        if path:
            default_storage.delete('storage_test')
            self.get_helper().unset(self.id, f'files.{name}')



    def is_refund_available(self, user=None, check_logs=False, order_id=None):
        res = True
        if user and not user.has_perm('can_drive'):
            res = False

        # no refundLogs
        if check_logs:
            logs = self.get_param('refundLogs', default=[])
            for log in logs:
                if log.get('order_id') == order_id:
                    res = False

        if order_id:
            ll_data = self.get_order(order_id)
            if ll_data:
                for key in ['is_refund', 'is_void', 'is_chargeback']:
                    if ll_data.get(key) in ['yes', '1']:
                        return False
        return res

    @property
    def is_chargebacked(self):
        return 'Chargebacked' in self.tags

    def get_refund_url(self):
        return reverse('refund', kwargs={'id': self.id})

    def get_param(self, first, second=None, default='', force_default=False):
        if '.' in first:
            first, second = first.split('.')
        res = self.d.get(first, {})
        if second:
            res = res.get(second, {})

        if res == {}:
            res = default

        if force_default:
            if res == '':
                res = default
        return res

    def time_difference(self):
        sent_info = self.get_param('orderInfo.emailSentInfo', default=None)
        diff = None
        time_created = self.get_param('created', default=None)
        if sent_info and isinstance(sent_info[0], dict) and time_created:
            time_sent = sent_info[0].get('dt')
            diff = time_sent - time_created
            if diff < datetime.timedelta(seconds=1):
                time_sent = time_sent + datetime.timedelta(hours=4)
                diff = time_sent - time_created
            # diff = "%s - %s, %s" % (time_sent, time_created, time_sent - time_created)
        return diff

    def sign_string(self):
        from django.core import signing
        id = "{mongo_id}_{email}".format(mongo_id=self.id, email=self.email)
        res = signing.dumps(id)
        return res

    def refund(self, sum_refund, order_id=None, fake=False):
        from mongo_admin.main import forms
        if self.is_refund_available():
            if order_id is None:
                order_id = self.order_id
            # sum_refund = float(self.get_param('orderInfo.amountPaid')) - float(minus_amount)
            ll = LimeLight()

            if fake:
                resp = True
            else:
                resp = ll.do_refund(sum_refund, order_id)

            if resp:
                # save to log
                helper = self.get_helper()
                helper.append_tag(self.id, 'Refunded')

                # refund amount, datetime.
                dSent = {
                    # 'charge': float(minus_amount),
                    'amount': sum_refund,
                    'order_id': order_id,
                    'dt': timezone.now()
                }

                if sum_refund > 200: 
                    self.set_filing_status(forms.FILING_CANCELLED)

                self.analytics_refund(sum_refund)
                helper.add_refund_log(self.id, dSent)

                return "Order {} refunded {}$".format(order_id, sum_refund)
            else:
                return "Error: error in limelight api call"
        else:
            return "Error: cant do refund, because it was made previously"

    def full_refund(self):
        all_orders = self.get_orders()
        return self.refund_orders(all_orders)

    def refund_orders(self, orders):
        sum_refund = 0
        logger.debug('found %s orders' % len(orders))
        if orders:
            for order in orders:
                if self.is_refund_available(order_id=order['id']):
                    logger.debug(order)
                    order['sum'] = int(float(order['sum']))
                    fake_refund = settings.FAKE_FULL_REFUND == 'True'
                    res = self.refund(order['sum'], order['id'], fake=fake_refund)
                    if not 'error' in res:
                        sum_refund += order['sum']
        return sum_refund

    def refund_delivery(self):
        delivery_orders = self.get_delivery_order()
        return self.refund_orders(delivery_orders)

    def discount_order(self, sum):
        ll = LimeLight()
        resp = ll.discount_order(self.order_id, sum)

        if resp:
            # save to log
            helper = self.get_helper()
            helper.append_tag(self.id, 'Discounted')
        return "Order created"

    def order_link(self):
        i_order = self.order_id
        url = "https://oneclickawaytgc.limelightcrm.com/admin/orders.php?show_details=show_details&show_folder=" \
              "view_all&fromPost=1&act=&sequence=1&show_by_id=%s" % (
                  i_order
              )
        return "<a href='%s' target='_blank'>%s</a>" % (url, i_order)

    def prospect_link(self):
        i_prospect = self.get_param('orderInfo', 'prospectId')
        url = "https://oneclickawaytgc.limelightcrm.com/admin/placeorder.php?prospect_id=%s&campaignId=319" % (
            i_prospect
        )
        return "<a href='%s' target='_blank'>%s</a>" % (url, i_prospect)

    def phones(self):
        """
        businessInformation.phoneNumber
        payment.phone
        taxIdRecipientInformation.receipientPhoneEIN
        :return:
        """
        l_phones = [
            phone_format(self.get_param('businessInformation', 'phoneNumber')),
            phone_format(self.get_param('payment', 'phone')),
            phone_format(self.get_param('taxIdRecipientInformation', 'receipientPhoneEIN'))
        ]
        while '' in l_phones:
            l_phones.remove('')
        return list(set(l_phones))

    @property
    def phone_sms_format(self):
        return sms_format(self.phones()[0])

    @property
    def tags(self):
        return self.get_param('tags')

    def emails(self):
        """
        taxIdRecipientInformation.gjcwell@gmail.com
        payment.email
        :return:
        """
        l_emails = [
            self.get_param('taxIdRecipientInformation', 'receipientEmailEIN'),
            self.get_param('payment', 'email'),
        ]
        while '' in l_emails:
            l_emails.remove('')
        return list(set(l_emails))

    @property
    def first_name(self):
        return self.get_param('personalInformation', 'firstName')

    @property
    def last_name(self):
        return self.get_param('personalInformation', 'lastName')

    @property
    def order_id(self):
        return self.get_param('orderInfo', 'orderId')

    @property
    def email(self):
        return self.emails()[0] if self.emails else None

    @property
    def filing_status(self):
        filing_status = self.get_param('orderInfo.filingStatus')
        if filing_status == 'New':
            filing_status = 'PROCESSING'
        return filing_status

    def get_row_class(self):
        tags = self.get_param('tags').lower()
        if 'done' in tags:
            return "table-success"
        elif 'refunded' in tags:
            return 'table-primary'
        elif 'ss4 faxed' in tags:
            return 'table-info'
        elif 'ss4' in tags and 'emailed' in tags:
            return 'table-orange'
        elif 'ss4' in tags:
            return 'table-warning'
        return ''

    def add_tag(self, tag_name):
        helper = self.get_helper()
        helper.append_tag(self.id, tag_name)

    def add_email_log(self, sEmailType):
        helper = self.get_helper()
        dSent = {
            'email_type': sEmailType,
            'to': self.email,
            'dt': timezone.now()
        }
        return helper.add_email_log(self.id, dSent)

    def set_filing_status(self, status):
        return self.get_helper().set_filing_status(self.id, status)

    def send_letter(self):
        result = lob.send_letter(self)
        self.get_helper().add_letter_log(self.id, result)
        if result.get('result'):
            self.add_tag('letter sent')
        return result

    def send_ein_sms(self):
        result = False
        if not 'sms' in self.tags.lower():
            result = sms.send_ein_sms(entry=self)
            if result:
                self.get_helper().add_sms_log(self.id, result)
                self.add_tag('sms')
            else:
                self.get_helper().add_sms_log(self.id, 'not send, some error')
        return result

    def get_short_ein_url(self):
        url = "https://{domain}/d/{code}".format(**{
            'domain': self.constants['domain'],
            'code': self.get_or_gen_short_code()
        })
        return url


    @property
    def amount(self):
        amount = self.get_param('orderInfo.amountPaid', default=0)
        try:
            return float(amount)
        except:
            return 0


    def get_type(self):
        TYPES = {
            247.0: {'name': 'USUAL'},
            277.0: {'name': '1DAY'},
            297.0: {'name': '1HOUR'},
        }
        return TYPES.get(self.amount, {})

    def send_fax(self, cd, to):
        # if we have receivedID
        result = {}
        signature_id = self.get_param('signatureId', [])[0]
        if signature_id:
            from mongo_admin.services.pyhellofax.api import HelloFaxAPI
            api = HelloFaxAPI(email=settings.FAX_EMAIL, password=settings.FAX_PASSWORD,
                              guid=settings.FAX_GUID)
            pdf = get_signature_request_file(signature_id, file_type='zip')
            if pdf:
                try:
                    if settings.FAX_TESTING == 'True':
                        result = json.loads(b'{"Transmission":{"Guid":"4c3a864a0f087fedf3c2607b85a0a3fe3cde9fa8","To":"14439142380","From":"14439142380","IsInbound":false,"IsDraft":false,"TypeCode":"F","StatusCode":"P","ErrorCode":null,"CreatedAt":"1534770735","UpdatedAt":"1534770735","Uri":"\\/v1\\/Accounts\\/fef2fd91ed1c62e92a871b4950f5716aa4280315\\/Transmissions\\/4c3a864a0f087fedf3c2607b85a0a3fe3cde9fa8","NumPagesBilled":"TESTED"}}')
                    else:
                        params = {
                            'CoverPageTo': cd.get('cover_page_to'),
                            'CoverPageFrom': cd.get('cover_page_from'),
                            'CoverPageMessage': cd.get('cover_page_message'),
                            'To': to,
                            'file': pdf
                        }
                        logger.info(params)
                        result = json.loads(api.send_fax(**params))
                    # save logs
                    self.get_helper().add_fax_log(self.id, result)
                except HelloFaxAPIException as e:
                    result['error'] = 'Fax API exception: %s' % str(e.get('message') if isinstance(e, dict) else e)
                except:
                    traceback.print_exc()
                    result['error'] = 'exception, need developer'


                if 'Transmission' in result:
                    self.add_tag('fax sent')
                    from mongo_admin.main.forms import FILING_WAITING_IRS
                    self.set_filing_status(FILING_WAITING_IRS)

            return result
        return result


    def _get_letter_id(self):
        try:
            return self.get_param('orderInfo.letterSentInfo')[0]['id']
        except:
            return ''

    def get_tracking_events(self):
        result = {}
        if self.get_param('orderInfo.letterSentInfo'):
            result = lob.get_details(self._get_letter_id()).get('tracking_events', [])
            # TODO make save_tracking_events
        return result

    def get_tracking_number(self):
        tracking_number = self.get_param('orderInfo.tracking_number', default=False)
        if not tracking_number or tracking_number[0] is None:
            tracking_number = lob.get_details(self._get_letter_id()).get('tracking_number')
            self.get_helper().save_tracking_number(self.id, tracking_number)
        return tracking_number

    def is_ready_for_send_ein(self, delay_minutes=30):
        # delay should be at least 30 minutes
        created = self.get_param('orderInfo.created', default=None)
        if created:
            dif_minutes = (timezone.now() - created).total_seconds() // 60
            logger.debug(dif_minutes)
            if dif_minutes < delay_minutes:
                return True
        return False

    def get_ein_template(self):
        default_template = 'emails/ein_ready/default.html'
        templates = {
            'TRUST': 'emails/ein_ready/trust.html',
            'LIMITED_LIABILITY_COMPANY': 'emails/ein_ready/llc.html',
            'ESTATE_DECEASED_INDIVIDUAL': 'emails/ein_ready/estate.html',
            'NON_PROFIT_ORGANIZATION': 'emails/ein_ready/non_profit.html',
            'PARTNERSHIP': 'emails/ein_ready/partnership.html',
            'CORPORATION': 'emails/ein_ready/corp.html',
            'SOLE_PROPRIETOR': 'emails/ein_ready/sole_prop.html',
            'CHURCH_CONTROLLED_ORGANIZATION': 'emails/ein_ready/church.html',
            'PERSONAL_SERVICE_CORPORATION': 'emails/ein_ready/personal.html'
        }
        entry_type = self.get_param('filingDocumentType')
        return templates.get(entry_type, default_template)

    def save_email(self, data, send_in):
        data['created'] = timezone.now()
        data['send_at'] = self.get_param('orderInfo.created') + datetime.timedelta(minutes=send_in)
        self.get_helper().save_email(self.id, data)
        logger.debug('email saved')
        return "Sending the email was queued"

    def send_queued_email(self):
        from mongo_admin.main.forms import EinEmailForm
        data = self.get_param('queuedEmail', {})
        if data.get('bcc'):
            data['bcc'] = ",".join(data.get('bcc', []))
        logger.debug('data - %s' % data)
        form = EinEmailForm(data, id=self.id)
        logger.debug('form generated')
        if form.is_valid():
            logger.debug('form valid')
            form.send_email(None)
            logger.debug('email send')
            self.get_helper().queued_email_sent(self.id)
            logger.debug('log edited')
        else:
            logger.error('form is not valid')
            logger.error(form.errors)

    def get_orders(self):
        ll = LimeLight()
        if self.orders is None:
            orders = ll.get_orders(self.email,
                      main_order=self.order_id)
            self.orders = orders

        return self.orders

    def get_order(self, order_id):
        orders = self.get_orders()
        for order in orders:
            if order['id'] == str(order_id):
                return order
        return None

    def get_delivery_order(self):
        usual_sum = 247
        orders = self.get_orders()
        delivery_orders = []
        if len(orders) == 1:
            main_order = orders[0]
            if main_order['sum'] > usual_sum:
                delivery_sum = main_order['sum'] - usual_sum
                delivery_order = dict(main_order)
                delivery_order['sum'] = delivery_sum
                delivery_orders = [delivery_order]
        else:
            delivery_orders = [order for order in orders if float(order['sum']) < 100]

        return delivery_orders

    def gen_short_code(self):
        return User.objects.make_random_password(4)

    def get_or_gen_short_code(self):
        code = self.get_short_code()
        if not code:
            exist_codes = self.get_exist_codes()
            while True:
                code = self.gen_short_code()
                if code not in exist_codes:
                    break
            self.get_helper().save_short_code(self.id, code)

        return code

    def get_short_code(self):
        return self.get_param("short_code", default="")

    def get_exist_codes(self):
        return list(self.get_helper()._entries().find({"short_code": {"$exists": True}}).distinct('short_code'))

    def gen_short_url(self):
        url = "https://{}/d/{}".format(self.constants['domain'], self.get_or_gen_short_code())
        return url

    def get_sum_refunded(self):
        refund_sum = 0
        logs = self.get_param("refundLogs")
        if logs:
            for log in logs:
                if log.get('amount'):
                    refund_sum += int(float(log['amount']))
        return refund_sum

    def get_analytics_sku(self):
        analytics_sku_dict = {
            "CHURCH_CONTROLLED_ORGANIZATION": "church-organization-application",
            "CORPORATION": "corporation-application",
            "ESTATE_DECEASED_INDIVIDUAL": "estate-application",
            "LIMITED_LIABILITY_COMPANY": "limited-liability-company-application",
            "NON_PROFIT_ORGANIZATION": "non-profit-application",
            "PARTNERSHIP": "partnership-application",
            "PERSONAL_SERVICE_CORPORATION": "personal-service-application",
            "SCORPORATION": "scorporation-application",
            "SOLE_PROPRIETOR": "sole-proprietor-application",
            "TRUST": "trust-application",
        }
        return analytics_sku_dict.get(self.get_param("filingDocumentType"), "")

    def analytics_refund(self, sum_refund):
        res = google_analytics.refund_analytics_transaction(
            sum=sum_refund,
            product_sku=self.get_analytics_sku(),
            order_id=self.order_id,
            client_id=self.get_client_id()
        )
        if res:
            self.get_helper().add_refund_log(self.id, {"message": "send to ga", "sum": sum_refund})
        return res

    def get_client_id(self):
        return self.get_param('additionalInformation.gaClientId', default=None)

    def get_error_message(self):
        # TODO make it quicker or save info to mongo
        from mongo_admin.entries.models import EINRetrievalLogStep
        errors = EINRetrievalLogStep.objects.filter(log__entry__mongo_id=self.id, success=False).values('number', 'error_messages_legacy')
        if errors:
            error = errors[0]
            error_msg = "{}. {}".format(error['number'], error['error_messages_legacy'])
            return error_msg
        return ''


class PdfHelper(object):
    def __init__(self, id):
        entry = PyMongoHelper().get_entry_by_id(id)
        # entry['payment'] = d_payments.get(str(entry['_id']), {})
        self.entry = entry

    def get_filename(self):
        entry = self.entry
        filename = slugify("{IRS_NAME}-{IRS_EIN}".format(**{
            'IRS_NAME': entry.get_param('orderInfo.irsName'),
            'IRS_EIN': entry.get_param('orderInfo.irsEin')
        })) + '.pdf'
        return filename

    def get_file(self, template_name="pdf/entry.html", **kwargs):
        pdf = generate_pdf(
            template_name,
            context={'e': self.entry, 'constants': self.entry.constants, 'date': datetime.date.today()},
            **kwargs)
        return pdf


class CustomFieldsHelper(object):
    def prepare_custom_fields(self, id):
        e = self.get_entry(id)

        custom_fields = {
            "1. Entity Legal Name": "personalInformation.firstName personalInformation.lastName",
            "2. Trade Name": "businessInformation.tradeName",
            "3. Executor, admin, trustee \"care of\" name": "personalInformation.firstName personalInformation.lastName",
            "4a. Mailing address": "businessInformation.address",
            "4b. city, state, zip": "businessInformation.city businessInformation.state businessInformation.zip",
            "5a. Street Address": "",
            "5b. city, state, zip code": "",
            "6. Business County and State": "businessInformation.county, businessInformation.state",
            "7a. Name of responsible party": "personalInformation.firstName personalInformation.lastName",
            "7b. SSN, ITIN, or EIN": "personalInformation.socialSecurity",  #
            "11. Date Business started or acquired": "entityStartDate",  # 01/15/2015
            '12. closing month of accounting year': calendar.month_abbr[
                int(e.get_param('entityAccountingClosingMonth', default=12))],
            "13. agricultural": e.get_param('additionalInformation.expectedNumberEmployees', default=0, force_default=True),
            "13. household": e.get_param('additionalInformation.expectedHouseHoldEmployees', default=0, force_default=True),
            "13. other": e.get_param('additionalInformation.otherEmployees', default=0, force_default=True),
            "14. over 1k annual tax": "additionalInformation.expectEmploymentTaxLiability",
            "15. first date wages paid": "additionalInformation.dateWagePaid",
            "17. merch sold, construction work, products made, services provided": "additionalInformation.specificProducts",
            "3rd Party Name": e.constants.get('company_name'),
            "3rd Party Phone": "888-675-9395",  #
            "3rd Party Address": "501 NE Hood Ave Gresham, OR 97030",  #
            "Applicant Phone #": e.phones()[0],  #
            "name_and_title": "personalInformation.firstName personalInformation.lastName (personalInformation.title)"
        }

        if e.get_param('filingDocumentType') == 'ESTATE_DECEASED_INDIVIDUAL':
            if e.get_param('deceasedInformation'):
                # new type of data
                custom_fields.update({
                    '1. Entity Legal Name': "deceasedInformation.firstName deceasedInformation.lastName ESTATE",
                    "3. Executor, admin, trustee \"care of\" name": 'personalInformation.firstName personalInformation.lastName',
                    "9a. Estate (SSN of decedent)": True,
                    "9a. Estate SSN": "deceasedInformation.socialSecurity",
                    "name_and_title": "personalInformation.firstName personalInformation.lastName (personalInformation.title)"
                })
            else:
                # old type of data
                custom_fields.update({
                    '1. Entity Legal Name': "personalInformation.firstName personalInformation.lastName ESTATE",
                    "3. Executor, admin, trustee \"care of\" name": 'representativeInformation.firstName representativeInformation.lastName',
                    "9a. Estate (SSN of decedent)": True,
                    "9a. Estate SSN": "personalInformation.socialSecurity",
                    "name_and_title": "representativeInformation.firstName representativeInformation.lastName (representativeInformation.title)"
                })
        elif e.get_param('filingDocumentType') == 'SOLE_PROPRIETOR':
            custom_fields["9a. Type (Sole proprietor SSN)"] = True
            custom_fields["9a. Sole Proprietor SSN"] = "personalInformation.verifySocialSecurity"
            custom_fields["8a. For LLC no"] = True

        elif e.get_param('filingDocumentType') == 'TRUST':
            custom_fields.update({
                "1. Entity Legal Name": "trustInformation.legalEntityName",
                "3. Executor, admin, trustee \"care of\" name": "representativeInformation.firstName representativeInformation.lastName",
                "9a. Trust (TIN of grantor)": True,
                "9a. Trust (TIN of grantor) text": "personalInformation.socialSecurity",
                "10. Reason - created trust": True,
                "10. Reason - Created Trust (specify)": "trustInformation.typeOfTrust",
                "name_and_title": "representativeInformation.firstName representativeInformation.lastName (representativeInformation.title)"
            })

        elif e.get_param('filingDocumentType') == 'CORPORATION':
            custom_fields.update({
                '1. Entity Legal Name': 'businessInformation.name',
                '9a. Corporation': True,
                '9a. Corp form number to be filed': '1120',
                '9b. If corp - state': 'businessInformation.state'
            })

        elif e.get_param('filingDocumentType') == 'LIMITED_LIABILITY_COMPANY':
            custom_fields.update({
                '1. Entity Legal Name': 'llcInformation.llcName',
                "8a. For LLC Yes": True,
                "8b. If 8a. yes, number of LLC members": "llcInformation.llcMembers",  #
                "8b. if 8a yes was LLC organized in USA (Yes)": True,
                "9a. other": True
            })
        elif e.get_param('filingDocumentType') == 'NON_PROFIT_ORGANIZATION':
            custom_fields.update({
                '1. Entity Legal Name': 'businessInformation.name',
                '9a. other nonprofit': True,
                '9a. non-profit (specify)': 'additionalInformation.primaryActivity',
            })

        """
        f otherbusmailinfo exists:
           write otherbusmailinfo in 4a and businessInformation.address in 5a
        else:
            businessInformation.address type in 4a, 5a is blank
        right?
        """
        if e.get_param('otherBusinessMailingInformation.address'):
            custom_fields.update({
                "4a. Mailing address": "otherBusinessMailingInformation.address",
                "4b. city, state, zip": "otherBusinessMailingInformation.city otherBusinessMailingInformation.state otherBusinessMailingInformation.zip",
                "5a. Street Address": "businessInformation.address",
                "5b. city, state, zip code": "businessInformation.city businessInformation.state businessInformation.zip",
            })

        # 10
        if e.get_param('additionalInformation.reasonForApplying') == 'banking_purposes':
            custom_fields.update({'10. Reason - banking purposes': True,
                                  '10. Reason - banking purposes (specify)': 'Banking'})
        elif e.get_param('additionalInformation.reasonForApplying') == 'started_business':
            custom_fields.update({'10. Reason - New business': True,
                                  '10 - specify new business type': 'additionalInformation.specificProducts'})
        elif e.get_param('additionalInformation.reasonForApplying') == 'changed_organization':
            custom_fields.update({'10. Reason - specify changed corp': 'additionalInformation.specificProducts'})

        d_reason = {
            'irs_compliance': '10. Reason - Compliance with IRS',
            'hired_employees': '10. Reason - hired employees (check box and see line 13)',
            'created_trust': '10. Reason - created trust',
            'started_business': '10. Reason - New business',
            'banking_purposes': '10. Reason - banking purposes',
            'purchased_business': '10. Reason - purchased going business',
            'changed_organization': '10. Reason - changed type of org',
        }
        if e.get_param('additionalInformation.reasonForApplying') in d_reason:
            custom_fields.update({d_reason[e.get_param('additionalInformation.reasonForApplying')]: True})

        # 16
        l_sixteen_fields = [
            "16. other",
            "16. health care/social assistance",
            "16. wholesale-agent/broker",
            "16. Construction",
            "16. rental & leasing",
            "16. transport & warehouse",
            "16. accommodation/food service",
            "16. wholesale-other",
            "16. Retail",
            "16. real estate",
            "16. manufacturing",
            "16. finance & insurance"
        ]

        # usual activities = ['Health Care', 'Retail', 'Accommodation', 'Other', 'Rental & Leasing', 'Transportation',
        # 'Social Assistance', 'Construction', 'Insurance', 'Food Service', 'Finance', 'Real Estate', 'Manufacturing',
        # 'Wholesale', 'Warehousing', 'Otro', 'real_estate', 'finance', 'transportation', 'rental_leasing',
        # 'construction', 'manufacturing', 'wholesale_other', 'social_assistance', 'other', 'retail', 'accommodation',
        # 'wholesale_agent', '', 'accommodations', 'wholesale', 'food_service', 'warehousing', 'health_care']
        s_primary_activity = e.get_param('additionalInformation.primaryActivity')
        s_activity_key = get_first_word(s_primary_activity)
        s_field_name = next((line for line in l_sixteen_fields if s_activity_key in line.lower()), "16. other")
        custom_fields.update({s_field_name: True})
        if s_field_name == "16. other": # and s_activity_key != 'other':
            custom_fields.update({"16. other (specify)": e.get_param('additionalInformation.specificProducts')})

        for key, val in custom_fields.items():
            custom_fields[key] = self.fill_string(val, e)

        # make title
        # for key, val in custom_fields.items():
        #     custom_fields[key] = self.capitalize(val)

        return custom_fields

    def get_entry(self, id):
        helper = PyMongoHelper()
        d_payments = helper.payments_dict()
        entry = helper._entries().find_one({"_id": ObjectId(id)})
        entry['payment'] = d_payments.get(str(entry['_id']), {})
        entry['id'] = str(entry['_id'])
        e = EntryWrapper(entry)
        return e

    def fill_string(self, line, e):
        if isinstance(line, str):
            elems = re.findall(r'([\w.]+)', line)
            for elem in elems:
                el = e.get_param(elem, default=None if not '.' in elem else '')
                if el is not None:
                    if isinstance(el, datetime.datetime):
                        el = el.strftime("%B %d, %Y")
                    elif el is False:
                        el = ''
                    elif el is True:
                        el = '1'
                    else:
                        if len(elems) <= 3 and str(el).lower() == str(el):
                            el = str(el).title()
                        # print(377, el)
                    line = line.replace(elem, el)

            line = line.replace('  ', ' ').strip()
        return line

    def capitalize(self, val):
        pass
        # if it's text
        # strip by words
        # if word is lower - then make it Index()


def clean_payment(self, id):
    helper = PyMongoHelper()
    # read all payments
    all_payments = helper.payments().find()
    for d_payment in all_payments:
        fixed_dict = {}
        if 'cvv' in d_payment:
            fixed_dict['cvv'] = '*' * 3
        if 'creditCardNumber' in d_payment:
            fixed_dict['creditCardNumber'] = '*' * (len(d_payment['creditCardNumber']) - 4) \
                                            + d_payment['creditCardNumber'][-4:]

        if 'expirationDate' in d_payment:
            fixed_dict['expirationDate'] = '*' * 4

        print(d_payment['document_id'], fixed_dict)

        # save result
        # if fixed_dict:
        #     helper.payments().update_one({
        #     '_id': d_payment['_id']
        #     }, {
        #         '$set': fixed_dict
        #     }, upsert=False)
        break

    # change

    # save
    # helper.payments().

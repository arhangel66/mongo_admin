import pprint
import re
from datetime import datetime, timedelta, date
from json import dumps
from random import randint

import pymongo
from bson import ObjectId
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.cache import cache
from django.db import transaction
from django.db.models import Q
from django.forms import formset_factory
from django.http import HttpResponseRedirect, HttpResponse, HttpRequest, JsonResponse
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView, FormView

from mongo_admin.core.db import PyMongoHelper
from mongo_admin.entries.models import Entry, SSNValidationLog, EINRetrievalLog, EINRetrievalLogStep, \
    EINVerificationLog, EINVerificationLogStep
from mongo_admin.limelight.api import LimeLight
from mongo_admin.main import forms
from mongo_admin.main.forms import EntryFilterForm, EntryEditForm, EmailForm, SignEmailForm, RefundForm, DiscountForm, \
    SendLetterForm, SendFaxForm, EinEmailForm
from mongo_admin.main.utils import EntryWrapper, PdfHelper, get_exist_tags, CustomFieldsHelper
from mongo_admin.services.fill_forms_irs import FillFormsIRSAPI


class ListObjects(PyMongoHelper, LoginRequiredMixin, TemplateView):
    template_name = "pages/mongo_list.html"
    is_show_test = False

    # def get(request):
    #     retur
    def get_entries(self):
        qs = {"orderInfo.paymentStatus": "Success"}
        if self.form.is_valid():
            cd = self.form.cleaned_data
            self.is_show_test = cd.get('show_test')

            qs = {}
            if cd.get('payment_status') in ['Success', 'Declined', "Lead"]:
                qs = {"orderInfo.paymentStatus": cd['payment_status']}
            elif cd['payment_status'] in ['None']:
                qs = {"orderInfo.paymentStatus": ''}

            if cd.get('filing_status') != "Any":
                qs.update({"orderInfo.filingStatus": cd['filing_status']})

            if cd.get('email'):
                qs.update({"taxIdRecipientInformation.receipientEmailEIN": cd['email']})

            # if cd.get('order_id'):
            #     qs.update({"orderInfo.orderId": cd['order_id']})
            if cd.get('universal'):
                # qs.update({"orderInfo.orderId": cd['universal']})
                qs.update({'$or': [
                    {"orderInfo.orderId": re.compile('.*%s.*' % cd.get('universal'), re.IGNORECASE)},
                    {"personalInformation.firstName": re.compile('.*%s.*' % cd.get('universal'), re.IGNORECASE)},
                    {"personalInformation.lastName": re.compile('.*%s.*' % cd.get('universal'), re.IGNORECASE)},
                    {"taxIdRecipientInformation.receipientEmailEIN": re.compile('.*%s.*' % cd.get('universal'),
                                                                                re.IGNORECASE)},
                    {"taxIdRecipientInformation.receipientPhoneEIN": re.compile('.*%s.*' % cd.get('universal').
                                                                                replace('-', ''), re.IGNORECASE)},

                ]})

            if cd.get('social_valid'):
                qs.update({"socialSecurityValid": '1'})

            if cd.get('entry_type'):
                type_dict = {
                    forms.ENTRY_USUAL: '247',
                    forms.ENTRY_1DAY: '277',
                    forms.ENTRY_1HOUR: '297',
                }
                qs.update({"orderInfo.amountPaid": re.compile('.*%s.*' % type_dict[cd.get('entry_type')],
                                                              re.IGNORECASE)})

            # tags
            no_tags = []
            yes_tags = []
            tags_conditions = []
            if not self.is_show_test:
                no_tags.append('test')

            if cd.get('tags'):
                yes_tags.append(cd['tags'])

            if cd.get('hide_closed'):
                no_tags.append('done')
                no_tags.append('refunded')

            if no_tags:
                tags_conditions.append(
                    {"tags":
                         {"$not": re.compile('.*%s.*' % "|".join(no_tags), re.IGNORECASE)}
                     }
                )
            if yes_tags:
                tags_conditions.append(
                    {"tags": re.compile('.*%s.*' % "|".join(yes_tags), re.IGNORECASE)}
                )
            if tags_conditions:
                qs['$and'] = tags_conditions
        entries = self._entries().find(qs).sort([['orderInfo.created', pymongo.DESCENDING], ["_id", pymongo.DESCENDING]])
        return entries

    @staticmethod
    def prepare_page_numbers(paginator, iPageNum):
        iNumPages = paginator.num_pages
        if iNumPages <= 11 or iPageNum <= 6:  # case 1 and 2
            lPageNumbers = [x for x in range(1, min(iNumPages + 1, 12))]
        elif iPageNum > iNumPages - 6:  # case 4
            lPageNumbers = [x for x in range(iNumPages - 10, iNumPages + 1)]
        else:  # case 3
            lPageNumbers = [x for x in range(iPageNum - 5, iPageNum + 6)]
        if lPageNumbers[0] != 1:
            lPageNumbers = [1] + lPageNumbers
        if lPageNumbers[-1] != iNumPages:
            lPageNumbers.append(iNumPages)
        return lPageNumbers

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        d_payments = self.payments_dict()
        context['search_form'] = self.get_form(self.request)

        from django.core.paginator import Paginator
        all_entries = self.get_entries()

        # use paginator
        paginator = Paginator(all_entries, 50)
        iPageNum = int(self.request.GET.get('page', 1))
        paginator_entries = paginator.page(iPageNum)
        lPageNumbers = ListObjects.prepare_page_numbers(paginator, iPageNum)

        start_num = 50 * (iPageNum - 1)
        lEntries = []
        for i, entry in enumerate(paginator_entries):
            entry['id'] = entry['_id']
            entry['payment'] = d_payments.get(str(entry['_id']), {})
            entry['counter'] = start_num + i + 1
            lEntries.append(EntryWrapper(entry))

        context['entries'] = lEntries
        context['all_tags'] = [tag['_id'] for tag in get_exist_tags()]
        context['page'] = paginator_entries
        context['pages'] = lPageNumbers
        return context

    def get_form(self, request):
        if request.GET.get('payment_status') is not None:
            form = EntryFilterForm(request.GET)
        else:
            form = EntryFilterForm({'payment_status': 'Success', 'filing_status': 'Any'})
        self.form = form
        return form


class DetailEntry(PyMongoHelper, LoginRequiredMixin, FormView):
    template_name = "pages/mongo_detail.html"
    form_class = EntryEditForm
    success_url = ""

    def get_initial(self):
        entry = EntryWrapper(self.get_entry())
        d_order = entry.d.get('orderInfo', {})
        initial = {
            'id': entry.id,
            'notes': d_order.get('notes', ''),
            'filing_status': d_order.get('filingStatus', ''),
            'payment_status': d_order.get('paymentStatus', ''),
            'irs_name': d_order.get('irsName', ''),
            'irs_ein': d_order.get('irsEin', ''),
            'email_sent': d_order.get('emailSent', ''),
            'firstName': entry.get_param('personalInformation.firstName'),
            'lastName': entry.get_param('personalInformation.lastName'),
            'socialSecurity': entry.get_param('personalInformation.socialSecurity'),
            'title': entry.get_param('personalInformation.title'),
            'city': entry.get_param('businessInformation.city'),
            'address': entry.get_param('businessInformation.address'),
        }
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        entry = self.get_entry()
        entry_o = EntryWrapper(entry)
        entry_str = pprint.pformat(entry, indent=4)
        context['entry'] = EntryWrapper(entry)
        context['entry_str'] = entry_str
        entry['main'] = {
            'created': entry.get('created', ''),
            'entityAccountingClosingMonth': entry.get('entityAccountingClosingMonth', ''),
            'entityStartDate': entry.get('entityStartDate', ''),
            'filingDocumentType': entry.get('filingDocumentType', ''),
            'jointVenture': entry.get('jointVenture', ''),
        }
        context['e'] = entry

        filename = PdfHelper(entry['id']).get_filename()
        custom_ein = entry_o.get_file_dict('ein')
        if custom_ein:
            file_link = "<a href='%s'>%s</a>" % (custom_ein['url'], custom_ein['filename'])
        else:
            file_link = "<a href='%s'>%s</a>" % (reverse('pdf', kwargs={'id': entry['id']}), filename)

        email_ein_form_submit_button_id = f'btnEINSubmit_{randint(0, 999)}'
        context['email_ein_form'] = EinEmailForm(button_name="Send email",
                                              button_id=email_ein_form_submit_button_id,
                                              prefix='email'
                                                     '',
                                              initial=self.get_email_ein_form_initial(), file_link=file_link,
                                              id=entry['id'])
        context['email_ein_form_submit_button_id'] = email_ein_form_submit_button_id

        context['email_error_form'] = EmailForm(button_name="Send email", prefix='email',
                                                initial=self.get_email_error_form_initial(),
                                                id=entry['id'])
        context['email_ss4_form'] = EmailForm(button_name="Send email", prefix='email',
                                              initial=self.get_ss4_required_form_initial(),
                                              id=entry['id'])
        context['real_letter_form'] = SendLetterForm(id=entry['id'])
        context['send_fax_form'] = SendFaxForm(id=entry['id'], initial=self.get_fax_form_initial())
        context['custom_fields'] = CustomFieldsHelper().prepare_custom_fields(entry['id'])
        context['email_ss4_sign_form'] = SignEmailForm(button_name='Send email', prefix='email',
                                                       initial=self.get_ss4_sign_form_initial(),
                                                       id=entry['id'])

        mongo_id_q = Q(entry__mongo_id=entry['id'])
        context['ssn_validation_logs'] = SSNValidationLog.objects.filter(mongo_id_q)
        context['ein_verification_logs'] = EINVerificationLog.objects.filter(mongo_id_q)
        context['ein_retrieval_logs'] = EINRetrievalLog.objects.filter(mongo_id_q)
        context['log_testing'] = settings.LOB_TESTING
        context['fax_testing'] = settings.FAX_TESTING

        return context

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(reverse("detail", kwargs={"id": form.cleaned_data['id']}))

    def get_email_ein_form_initial(self):
        entry = EntryWrapper(self.get_entry())

        initial = {
            'email_type': "send ein",
            'subject': 'EIN registration details!',
            'to': ", ".join(entry.emails()),
            'bcc': ", ".join(entry.domain_config.EMAIL_BCC),
            'body': render_to_string(entry.get_ein_template(), context={"e": entry.d, "constants": entry.constants}),
            'attachment': '',
            'id': entry.d['id']
        }
        return initial

    def get_email_error_form_initial(self):
        entry = EntryWrapper(self.get_entry())

        initial = {
            'email_type': "ssn error",
            'subject': "YOUR EIN APPLICATION",  # "There may be an error in your EIN application",
            'to': ", ".join(entry.emails()),
            'bcc': ", ".join(entry.domain_config.EMAIL_BCC),
            'body': render_to_string("emails/ssn_new_error.html", context={"e": entry.d, "constants": entry.constants}),
            'attachment': '',
            'id': entry.d['id']
        }
        return initial

    def get_ss4_required_form_initial(self):
        entry = EntryWrapper(self.get_entry())

        initial = {
            'email_type': "ss4 required",
            'subject': "**Urgent** Regarding your EIN application",
            'to': ", ".join(entry.emails()),
            'bcc': ", ".join(entry.domain_config.EMAIL_BCC),
            'body': render_to_string("emails/ss4_required.html", context={"e": entry.d, "constants": entry.constants}),
            'attachment': '',
            'id': entry.d['id']
        }
        return initial

    def get_ss4_sign_form_initial(self):
        entry = EntryWrapper(self.get_entry())
        sName = "%s %s" % (entry.first_name, entry.last_name)
        initial = {
            'email_type': "ss4 sign",
            'subject': "%s SS-4" % sName,
            'to': ", ".join(entry.emails()),
            'body': render_to_string("emails/ss4_sign.html", context={"e": entry.d, "constants": entry.constants}),
            'name': sName,
            'attachment': '',
            'id': entry.d['id']
        }
        return initial

    def get_fax_form_initial(self):
        entry = EntryWrapper(self.get_entry())
        context = {
            'from_fax': settings.FAX_FROM,
            'from_name': '',
            'to_fax': settings.FAX_TO,
            'to_name': 'Internal Revenue Service',
            'to_date': date.today()
        }
        initial = {
            'cover_page_to': render_to_string('fax/cover_page_to.html', context=context),
            'cover_page_from': render_to_string('fax/cover_page_from.html', context=context),
            'cover_page_message': render_to_string('fax/cover_page_message.html', context=context),
            'id': entry.d['id']
        }
        return initial


RefundFormSet = formset_factory(RefundForm)


class RefundEntry(PyMongoHelper, PermissionRequiredMixin, FormView):
    permission_required = 'can_refund'
    template_name = 'pages/entry_refund.html'
    form_class = RefundForm
    list_orders = None

    def handle_no_permission(self):
        messages.add_message(self.request, messages.ERROR, 'You have no permission for refund orders')
        return redirect('list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entry'] = EntryWrapper(self.get_entry())

        ll = LimeLight()
        list_orders = ll.get_orders(context['entry'].get_param('payment', 'email'),
                                    main_order=context['entry'].order_id)
        formset_initial = []
        for i, order in enumerate(list_orders):
            order['max_refund'] = order['sum'] - float(order['refund_amount'])
            form_initial = {
                'order_id': order['id'],
                'refund_charge': 0 if i == 0 else order['max_refund'],
                'refund_amount': order['max_refund'] if i == 0 else 0
            }
            formset_initial.append(form_initial)

        formset = RefundFormSet(initial=formset_initial)
        for i, order in enumerate(list_orders):
            order['form'] = formset[i]
        context['lOrders'] = list_orders
        context['formset'] = formset

        # formset = ArticleFormSet(initial=[])

        context['discount_form'] = DiscountForm(id=context['entry'].id)
        return context

    def post(self, request, *args, **kwargs):
        entry = EntryWrapper(self.get_entry())
        formset = RefundFormSet(request.POST, request.FILES)
        for form in formset:
            if form.is_valid():
                cd = form.cleaned_data
                if cd.get('refund_amount', 0) > 0:
                    refunded_result = entry.refund(cd['refund_amount'], order_id=cd['order_id'])
                    if not 'error' in refunded_result:
                        messages.add_message(self.request, messages.SUCCESS, refunded_result)
                    else:
                        messages.add_message(self.request, messages.WARNING, refunded_result)
        return HttpResponseRedirect(reverse("refund", kwargs={"id": entry.id}))


    def get_initial(self):
        initial = super().get_initial()

        entry = EntryWrapper(self.get_entry())
        return initial


class DiscountOrder(PyMongoHelper, PermissionRequiredMixin, FormView):
    permission_required = 'can_refund'
    form_class = DiscountForm

    def get(self, request, id):
        return HttpResponseRedirect(reverse("refund", kwargs={"id": id}))

    def form_valid(self, form):
        entry = EntryWrapper(self.get_entry())
        cd = form.cleaned_data
        refunded_result = entry.discount_order(cd['price'])
        if not 'error' in refunded_result:
            messages.add_message(self.request, messages.SUCCESS, refunded_result)
        else:
            messages.add_message(self.request, messages.WARNING, refunded_result)
        return HttpResponseRedirect(reverse("refund", kwargs={"id": entry.id}))


class LetterSendView(PyMongoHelper, FormView):
    form_class = SendLetterForm

    def get(self, request, id):
        return HttpResponseRedirect(reverse("refund", kwargs={"id": id}))

    def form_valid(self, form):
        entry = EntryWrapper(self.get_entry())
        letter_result = entry.send_letter()
        if not 'error' in letter_result:
            messages.add_message(self.request, messages.SUCCESS, 'Letter was sent')
        else:
            messages.add_message(self.request, messages.WARNING, letter_result['error'])
        return HttpResponseRedirect(reverse("detail", kwargs={"id": entry.id}))


class FaxSendView(PyMongoHelper, FormView):
    form_class = SendFaxForm

    def get(self, request, id):
        return HttpResponseRedirect(reverse("refund", kwargs={"id": id}))

    def form_valid(self, form):
        cd = form.cleaned_data
        entry = EntryWrapper(self.get_entry())
        fax_result = entry.send_fax(cd, to=settings.FAX_TO)

        if settings.FAX_DUPLICATE == 'True':
            entry.send_fax(cd, to=settings.FAX_FROM)

        if not 'error' in fax_result:
            messages.add_message(self.request, messages.SUCCESS, 'Fax was sent. It will appear in hellofax in few minutes')
        else:
            messages.add_message(self.request, messages.WARNING, fax_result['error'])
        return HttpResponseRedirect(reverse("detail", kwargs={"id": entry.id}))


class DelFile(View):
    def get(self, request, id, name):
        entry = PyMongoHelper().get_entry_by_id(id)
        entry.del_file(name)
        return redirect('detail', id=id)


class PdfEntry(View):
    template_name = "pdf/entry.html"

    def get(self, request, *args, **kwargs):
        """
        Handles GET requests and instantiates a blank version of the form.
        """
        resp = HttpResponse(content_type='application/pdf')
        pdf_helper = PdfHelper(kwargs.get('id'))
        if not request.GET.get('view'):
            resp['Content-Disposition'] = 'attachment; filename=%s' % pdf_helper.get_filename()

        result = pdf_helper.get_file(template_name=self.template_name, request=request, response=resp)

        return result

    def post(self, request, *args, **kwargs):
        from django.contrib import messages

        previx_dict = {
            "send ein": EinEmailForm,
            'ss4 sign': SignEmailForm
        }
        form_class = previx_dict.get(request.POST.get('email-email_type'), EmailForm)
        form = form_class(request.POST, request.FILES, id=request.POST.get('id'), prefix='email')

        if form.is_valid():
            # if it's send_ein and less 30 min - then save for send later
            entry = form.get_entry()
            delay_minutes = 30
            if form.is_it_ein() and entry.is_ready_for_send_ein(delay_minutes):
                res = entry.save_email(form.cleaned_data, send_in=delay_minutes)
            else:
                res = form.send_email(request)

            if not 'error' in res:
                messages.add_message(request, messages.SUCCESS, res)
            else:
                messages.add_message(request, messages.WARNING, res)
        else:
            messages.add_message(request, messages.WARNING, form.errors)
        return HttpResponseRedirect(reverse("list"))


class PdfEntryNew(PdfEntry):
    template_name = "pdf/entry_oficial.html"


class ValidateEntrySSNView(PyMongoHelper, LoginRequiredMixin, View):
    def post(
        self,
        request: HttpRequest,
        *args,
        **kwargs
    ) -> HttpResponse:
        id = request.POST.get('id')

        # save user to mongodb
        PyMongoHelper().set_user(id, request.user.username)

        ssn3, ssn2, ssn4 = request.POST.get('ssn').split('-')
        # TODO: handle invalid ssn format through bad http response
        first_name = request.POST.get('first_name')
        middle_name = request.POST.get('middle_name')
        last_name = request.POST.get('last_name')

        # TODO: SRP violation here -- refactor to service or smth
        result = FillFormsIRSAPI.validate_ssn(
            first_name=first_name,
            middle_name=middle_name,
            last_name=last_name,
            ssn3=ssn3,
            ssn2=ssn2,
            ssn4=ssn4,
        )

        if not result.get('success'):
            PyMongoHelper().set_filing_status(id, forms.FILING_SSN_ERROR)

        with transaction.atomic():
            entry, __ = Entry.objects.get_or_create(mongo_id=id)
            SSNValidationLog.objects.create(
                entry=entry,
                success=bool(result['success']),
                message=result['message'] or '',
                url=result['url'] or '',
                img_data=result['imgData'] or ''
            )

        return JsonResponse(data=result)


class EINVerificationLogsView(PyMongoHelper, LoginRequiredMixin, View):
    def post(
        self,
        request: HttpRequest,
        *args,
        **kwargs
    ) -> HttpResponse:
        id = request.POST.get('id')
        mongo_entry = self._entries().find_one({"_id": ObjectId(oid=id)})

        # TODO: SRP violation here -- refactor to service or smth
        result = FillFormsIRSAPI.verify_user_data(mongo_entry)
        with transaction.atomic():
            entry, __ = Entry.objects.get_or_create(mongo_id=id)
            entry_log = EINVerificationLog.objects.create(
                entry=entry,
                success=result['success'],
                message=result['message'] or '',
            )
            EINVerificationLogStep.objects.bulk_create([
                EINVerificationLogStep(
                    log=entry_log,
                    number=step_number + 1,
                    success=step['success'],
                    url=step['url'],
                    form_header=step['formHeader'],
                    error_messages_legacy=EINVerificationLogStep.ERROR_MESSAGES_LEGACY_DELIMITER.join(
                        step['errorMessages']),
                    problem_in_legacy=dumps(step['problemIn'] or ''),
                    img_data=step['imgData'],
                )
                for step_number, step in enumerate(result['steps'])
            ])

        return JsonResponse(data=result)


class EINRetrievalLogsView(PyMongoHelper, LoginRequiredMixin, View):
    def post(
        self,
        request: HttpRequest,
        *args,
        **kwargs
    ) -> HttpResponse:
        id = request.POST.get('id')
        mongo_entry = self._entries().find_one({"_id": ObjectId(oid=id)})

        # TODO: SRP violation here -- refactor to service or smth
        result = FillFormsIRSAPI.receive_ein(mongo_entry)
        with transaction.atomic():
            entry, __ = Entry.objects.get_or_create(mongo_id=id)
            entry_log = EINRetrievalLog.objects.create(
                entry=entry,
                success=result['success'],
                message=result['message'] or '',
                assigned_ein=result['assignedEIN'] or '',
                assigned_legal_name=result['assignedLegalName'] or '',
            )
            PyMongoHelper().set_user(id, request.user.username)

            entry_log.save_ein_in_mongo()
            retrievals = EINRetrievalLogStep.objects.bulk_create([
                EINRetrievalLogStep(
                    log=entry_log,
                    number=step_number + 1,
                    success=step['success'],
                    url=step['url'],
                    form_header=step['formHeader'],
                    error_messages_legacy=EINRetrievalLogStep.ERROR_MESSAGES_LEGACY_DELIMITER.join(
                        step['errorMessages']),
                    problem_in_legacy=dumps(step['problemIn'] or ''),
                    img_data=step['imgData'],
                )
                for step_number, step in enumerate(result['steps'])
            ])

            if retrievals and not retrievals[-1].success:
                entry = PyMongoHelper().get_entry(id, wrapper=True)
                PyMongoHelper().set_error_message(id, retrievals[-1].error_messages_legacy)
                entry.add_tag('ERROR')
                entry.set_filing_status(forms.FILING_OTHER_ERROR)

        return JsonResponse(data=result)


class Statistic(object):
    def get_full_stat(self):
        params = cache.get('full_stat')
        # if not params:
        dat = (datetime.now() - timedelta(hours=6)).date()
        params = {
            'date': dat,
            'revenue': self.get_revenue(dat),
        }
        cache.set('full_stat', 'hello, world!', 30)
        return params

    def get_revenue(self, dat):
        from mongo_admin.limelight.api import LimeLightApi
        api = LimeLightApi()
        return api.get_revenue(dat).get('revenue')

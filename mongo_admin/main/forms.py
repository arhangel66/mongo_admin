import traceback
from logging import getLogger

from bson import ObjectId
from ckeditor.widgets import CKEditorWidget
from crispy_forms.layout import Layout, HTML
from django import forms
from django.conf import settings
from django.contrib import messages
from django.core.mail import EmailMultiAlternatives
from django.forms import HiddenInput
from django.urls import reverse
from django.utils import timezone
from django.utils.html import strip_tags
from requests.exceptions import SSLError

from mongo_admin.core.db import PyMongoHelper
from mongo_admin.core.forms import CryspyInlineMixin, CryspyHorizontalMixin
from mongo_admin.main.utils import PdfHelper, get_exist_tags, CustomFieldsHelper

logger = getLogger(__name__)

REFUND_CHOICES = (
    ("0", "Full refund"),
    ("10", "Refund (charge $10)"),
    ("25", "Refund (charge $25)"),
    ("50", "Refund (charge $50)"),
    ("75", "Refund (charge $75)"),
    ("100", "Refund (charge $100)"),
)
PAYMENT_CHOICES = (
    ("any", 'payment status'),
    ("Success", "Success"),
    ("Lead", "Lead"),
    ("Declined", "Declined"),
    ("None", "None")
)

FILING_PROCESSING = "New"
FILING_WAITING_SS4 = "WAITING SS4 SIGNATURE"
FILING_SSN_ERROR = "SSN ERROR"
FILING_OTHER_ERROR = "OTHER ERROR"
FILING_WAITING_IRS = "WAITING FOR IRS"
FILING_CANCELLED = "CANCELLED"
FILING_COMPLETE = "COMPLETE"
FILING_LEAD = 'Lead'

ENTRY_USUAL = 'USUAL'
ENTRY_1DAY = '1DAY'
ENTRY_1HOUR = '1HOUR'

ENTRY_TYPES = (
    ('', 'Any'),
    (ENTRY_USUAL, 'Usual'),
    (ENTRY_1DAY, '1 Day'),
    (ENTRY_1HOUR, '1 Hour'),
)

FILING_CHOICES = (
    (FILING_PROCESSING, "Processing"),  # on all new orders
    (FILING_WAITING_SS4, "Waiting SS4 signature"),  # <- if SS4 sent
    (FILING_SSN_ERROR, "SSN error"),  # - if SSN sent
    (FILING_OTHER_ERROR, "Other error"),  # - marked manually
    (FILING_WAITING_IRS, "Waiting for IRS"),  # <- if faxed to SS4
    (FILING_CANCELLED, "Cancelled"),  # if refunded
    (FILING_COMPLETE, "Complete"),  # if EIN sent
    (FILING_LEAD, "Lead"),  # if order was not completed
)

FILING_CHOICES_SEARCH = (("Any", "Filing status"),) + FILING_CHOICES


class EntryFilterForm(CryspyInlineMixin, forms.Form):
    payment_status = forms.ChoiceField(choices=PAYMENT_CHOICES, label="Payment status", required=False)
    tags = forms.ChoiceField(choices=[], label="Tag", required=False)
    # order_id = forms.CharField(max_length=15, label="OrderId", required=False)
    universal = forms.CharField(max_length=25, label="OrderId/email/name/phone", required=False)
    # email = forms.CharField(max_length=100, label="mail", required=False)
    # social_valid = forms.NullBooleanField(label="social valid", required=False)
    show_test = forms.BooleanField(widget=forms.CheckboxInput, label='Show test', required=False)
    hide_closed = forms.BooleanField(widget=forms.CheckboxInput, label='Hide closed', required=False)
    entry_type = forms.ChoiceField(ENTRY_TYPES, label='Hide closed', required=False)

    filing_status = forms.ChoiceField(choices=FILING_CHOICES_SEARCH, label="Filing status", required=False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # update available tags and its stats every request
        self.fields['tags'].choices = self.get_tags_choices()

    def get_tags_choices(self):
        return [("", 'Choose tag')] + [(
            tag['_id'],
            '%s (%d)' % (tag['_id'], tag['count'])
        ) for tag in get_exist_tags()
        ]


PdfFileField = forms.ClearableFileInput(attrs={'accept': 'application/pdf'})


class EntryEditForm(CryspyHorizontalMixin, forms.Form):
    id = forms.CharField(widget=HiddenInput())
    notes = forms.CharField(required=False, max_length=1000, widget=forms.Textarea(attrs={'rows': 4}))
    irs_name = forms.CharField(required=False, max_length=100)
    irs_ein = forms.CharField(required=False, max_length=100)
    filing_status = forms.ChoiceField(choices=FILING_CHOICES, label="Filing status", required=False)
    payment_status = forms.ChoiceField(choices=PAYMENT_CHOICES, label="Payment status", required=False)
    # email_sent = forms.CharField(required=False, max_length=200)
    firstName = forms.CharField(label="First name", required=False, max_length=120)
    lastName = forms.CharField(label="Last name", required=False, max_length=120)
    socialSecurity = forms.CharField(label="Social security", required=False, max_length=120)
    title = forms.CharField(label="Title", required=False, max_length=120)
    city = forms.CharField(label="businessInformation.city", required=False, max_length=120)
    address = forms.CharField(label="businessInformation.address", required=False, max_length=120)
    ein = forms.FileField(label='Custom EIN pdf', required=False, widget=PdfFileField)
    articles = forms.FileField(label='Articles of Incorporation', required=False, widget=PdfFileField)
    certificate = forms.FileField(label="Certificate of Formation", required=False, widget=PdfFileField)

    def set_pdf_link(self, name, url, filename, del_url='', id=-2):
        file_line = f"""
                    <div id="div_id_email-attachment" class="form-group row"> 
                    <label for="id_email-attachment" class="form-control-label col-sm-3 col-form-label requiredField">
                        {name}
                    </label> 
                    <div class="col-sm-9"> 
                        {filename}<br>
                        <a href="{url}">view</a> | <a href="{del_url}">del</a> 
                    </div> 
                    </div>
                    """
        lLayout = self.helper.layout[:id] + [HTML(file_line), ] + self.helper.layout[id+1:]
        self.helper.layout = Layout(*lLayout)

    def save(self):
        cd = self.cleaned_data
        entries = PyMongoHelper()._entries()
        res = entries.update_one({
            '_id': ObjectId(cd['id'])
        }, {
            '$set': {
                'orderInfo.notes': cd['notes'],
                'orderInfo.filingStatus': cd['filing_status'],
                'orderInfo.irsName': cd['irs_name'],
                'orderInfo.irsEin': cd['irs_ein'],
                # 'orderInfo.emailSent': cd['emailSent'],
                'orderInfo.paymentStatus': cd['payment_status'],
                "personalInformation.firstName": cd['firstName'],
                "personalInformation.lastName": cd['lastName'],
                "personalInformation.socialSecurity": cd['socialSecurity'],
                "personalInformation.title": cd['title'],
            }
        }, upsert=False)

        # save files
        entry = PyMongoHelper().get_entry_by_id(cd['id'])
        for field in ['ein', 'articles', 'certificate']:
            if cd[field]:
                entry.save_file(field, cd[field])
                print(f"{field} saved")
        return True

    def __init__(self, *args, **kwargs):
        id = str(kwargs['initial']['id'])
        # id = kwargs.get('initial', {}).get('id')
        super().__init__(*args, **kwargs)
        self.helper.form_method = "POST"
        lLayout = self.helper.layout[:2] + \
                  [HTML("<h4>Order Info</h4>"), ] + \
                  self.helper.layout[2:6] + \
                  [HTML("<h4>Personal Info</h4>"), ] + \
                  self.helper.layout[6:9] + \
                  [HTML("<h4>Address Info</h4>"), ] + \
                  self.helper.layout[10:]

        self.helper.layout = Layout(*lLayout)

        entry = PyMongoHelper().get_entry_by_id(id)
        lines = [
            {'name': 'ein', 'id': -4},
            {'name': 'articles', 'id': -3},
            {'name': 'certificate', 'id': -2},
        ]
        for line in lines:
            file_dict = entry.get_file_dict(line['name'])
            if file_dict:
                del_url = reverse('del-file', kwargs={'id': entry.id, 'name': line['name']})
                self.set_pdf_link(line['name'], file_dict['url'], file_dict['filename'], del_url, line['id'])


class RefundForm(CryspyInlineMixin, forms.Form):
    # refund_percent = forms.ChoiceField(widget=forms.Select(), label="Choose Refund", choices=REFUND_CHOICES)
    order_id = forms.IntegerField(widget=forms.HiddenInput())
    refund_charge = forms.IntegerField(label="Charge refund", widget=forms.TextInput(attrs={'placeholder': 0, 'class': 'form-control'}))
    refund_amount = forms.IntegerField(required=True, widget=forms.TextInput(attrs={'readonly': 'readonly', 'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        kwargs['button_name'] = 'Yes, Refund'
        super().__init__(*args, **kwargs)
        self.helper.form_method = 'POST'


class DiscountForm(CryspyHorizontalMixin, forms.Form):
    price = forms.FloatField()

    def __init__(self, *args, **kwargs):
        kwargs['button_name'] = 'Order'
        entry_id = kwargs.pop('id', '')
        super().__init__(*args, **kwargs)
        self.helper.form_method = 'POST'
        if entry_id:
            self.helper.form_action = reverse('discount', kwargs={'id': entry_id})


class SendLetterForm(CryspyInlineMixin, forms.Form):
    entry_id = forms.HiddenInput()

    def __init__(self, *args, **kwargs):
        kwargs['button_name'] = 'Send letter'
        entry_id = kwargs.pop('id', '')
        super().__init__(*args, **kwargs)
        self.helper.form_method = 'POST'
        if entry_id:
            self.helper.form_action = reverse('letter_send', kwargs={'id': entry_id})


class SendFaxForm(CryspyHorizontalMixin, forms.Form):
    entry_id = forms.HiddenInput()
    cover_page_to = forms.CharField(widget=forms.Textarea(attrs={'rows': 4}))
    cover_page_from = forms.CharField(widget=forms.Textarea(attrs={'rows': 4}))
    cover_page_message = forms.CharField(widget=forms.Textarea(attrs={'rows': 4}))

    def __init__(self, *args, **kwargs):
        kwargs['button_name'] = 'Send Fax'
        entry_id = kwargs.pop('id', '')
        super().__init__(*args, **kwargs)
        self.helper.form_method = 'POST'
        if entry_id:
            self.helper.form_action = reverse('fax_send', kwargs={'id': entry_id})


class EmailForm(CryspyHorizontalMixin, forms.Form):
    id = forms.CharField(widget=HiddenInput())
    name = forms.CharField(widget=HiddenInput(), required=False)
    email_type = forms.CharField(widget=HiddenInput())
    subject = forms.CharField(label="Subject", required=True, max_length=200)
    to = forms.CharField(label='to', required=True, max_length=200)
    bcc = forms.CharField(label='bcc', required=False, max_length=300)
    body = forms.CharField(required=False, max_length=10000, widget=CKEditorWidget(config_name='awesome_ckeditor'))

    def add_pdf_link(self, file_link):
        file_line = """
                    <div id="div_id_email-attachment" class="form-group row"> 
                    <label for="id_email-attachment" class="form-control-label col-sm-3 col-form-label requiredField">
                        Pdf attachment<span class="asteriskField">*</span> 
                    </label> 
                    <div class="col-sm-9"> 
                        %s 
                    </div> 
                    </div>
                    """ % file_link
        lLayout = self.helper.layout[:4] + \
                  [HTML(file_line), ] + \
                  self.helper.layout[4:]

        self.helper.layout = Layout(*lLayout)

    def __init__(self, *args, **kwargs):
        file_link = kwargs.pop('file_link', '')
        id = kwargs.pop('id', '')
        super().__init__(*args, **kwargs)

        if file_link:
            self.add_pdf_link(file_link)
        self.helper.form_method = 'POST'
        self.helper.form_action = reverse('pdf', kwargs={'id': id})

    def is_it_ein(self):
        cd = self.cleaned_data
        if cd['email_type'] == 'send ein':
            return True
        return False

    def get_entry(self):
        return PyMongoHelper().get_entry_by_id(self.cleaned_data['id'])

    def clean_bcc(self):
        bcc = self.cleaned_data['bcc']
        if ',' in bcc:
            bcc = bcc.split(',')
        elif bcc == '':
            bcc = None
        elif isinstance(bcc, list):
            pass
        else:
            bcc = [bcc]

        if bcc:
            bcc = [email.strip() for email in bcc]
        return bcc

    def send_email(self, request):
        cd = self.cleaned_data

        if '<' in cd['body']:
            text_content = strip_tags(cd['body'])
            html_content = cd['body']
        else:
            text_content = cd['body']
            html_content = None


        email = EmailMultiAlternatives(
            cd['subject'],
            text_content,
            self.get_entry().domain_config.SUPPORT_EMAIL
            ,
            cd['to'].split(','),
            bcc=cd['bcc']
        )
        if cd.get('email_type') == 'send ein':
            entry = self.get_entry()
            ein = entry.get_file_dict('ein')
            if ein:
                filename = ein['filename']
                pdf = entry.get_file_obj('ein')
            else:
                pdf_helper = PdfHelper(cd['id'])
                pdf = pdf_helper.get_file(request=request)
                filename = pdf_helper.get_filename()
            email.attach(filename, pdf, "application/pdf")

        if html_content:
            email.attach_alternative(html_content, "text/html")

        res = email.send()
        if res:
            dSent = {
                'email_type': cd['email_type'],
                'to': cd['to'],
                'dt': timezone.now(),
            }
            helper = PyMongoHelper()
            helper.add_email_log(cd['id'], dSent)

            type_status_dict = {
                'send ein': FILING_COMPLETE,
                'ssn error': FILING_SSN_ERROR,
                'ss4 sign': FILING_WAITING_SS4
            }
            if cd.get('email_type') in type_status_dict:
                helper.set_filing_status(cd['id'], type_status_dict[cd.get('email_type')])

            sSent = "Email '%s' was sent to %s at %s" % (cd['email_type'], cd['to'], timezone.now())
            return sSent


class EinEmailForm(EmailForm):
    is_send_sms = forms.BooleanField(widget=forms.CheckboxInput, required=False, label='Send sms with link?',
                                  initial=True)
    is_send_letter = forms.BooleanField(widget=forms.CheckboxInput, required=False, label='Send real letter?',
                                     initial=True)

    def send_sms(self, request):
        entry = self.get_entry()
        result = entry.send_ein_sms()
        # result = sms.send_ein_sms(self.get_entry())
        if result:
            if settings.SMS_REAL_SENDING is 'True':
                msg = 'SMS was sent'
            else:
                msg = "SMS was saved to db. Not sent, because it's still testing"
            if request:
                messages.add_message(request, messages.SUCCESS, msg)
            return msg
        else:
            if request:
                messages.add_message(request, messages.WARNING, "sms was not sent")

    def send_letter(self, request):
        entry = self.get_entry()
        letter_result = entry.send_letter()
        if not 'error' in letter_result:
            if request:
                messages.add_message(request, messages.SUCCESS, 'Letter was sent')
        else:
            if request:
                messages.add_message(request, messages.WARNING, letter_result['error'])

    def send_email(self, request):
        res = super().send_email(request)
        if not 'error' in res:
            # send sms
            try:
                if self.cleaned_data['is_send_sms']:
                    self.send_sms(request)

                # send letter
                if self.cleaned_data['is_send_letter']:
                    self.send_letter(request)
            except:
                logger.exception('exception, when send sms or letter')
        return res

class SignEmailForm(EmailForm):
    name = forms.CharField(required=False, max_length=300)
    bcc = forms.CharField(label='bcc', required=False, max_length=300, widget=HiddenInput())
    refund_delivery = forms.BooleanField(widget=forms.CheckboxInput, required=False, label='Refund expedited charges?', initial=True)

    def send_email(self, request):
        cd = self.cleaned_data
        from hellosign_sdk import HSClient
        client = HSClient(api_key='da469e89bc17605274f4553244737d9ad71ebb99ff4849ef9436ac423c751e1e')
        template_id = '2355a49e456bac909aaab44b1471dec30cb59069'
        custom_fields = CustomFieldsHelper().prepare_custom_fields(cd['id'])
        p_d = request.POST.dict()
        post_fields = {k: p_d[k] for k in p_d if k in custom_fields}
        custom_fields.update(post_fields)
        try:
            signature_request = client.send_signature_request_with_template(
                # test_mode=True,  # TODO remove later
                template_id=template_id,
                title=cd['subject'],
                subject=cd['subject'],
                message=strip_tags(cd['body']),
                signing_redirect_url=None,
                signers=[{'role_name': 'Client', 'email_address': cd['to'], 'name': cd['name']}],
                custom_fields=[custom_fields]
            )
            if signature_request:
                sRes = "Email '%s' was sent to %s at %s" % (cd['email_type'], cd['to'], timezone.now())
                dSent = {
                    'email_type': cd['email_type'],
                    'to': cd['to'],
                    'dt': timezone.now(),
                    'signature_request_id': signature_request.signature_request_id
                }
                PyMongoHelper().add_email_log(cd['id'], dSent)
                PyMongoHelper().save_signature(cd['id'], signature_request.signature_request_id)
            else:
                sRes = 'error, not sent'
        except SSLError:
            sRes = 'error: connection, try later'
        except:
            sRes = 'other error, need developer'
            traceback.print_exc()

        if cd['refund_delivery']:
            entry = PyMongoHelper().get_entry_by_id(cd['id'])
            refunded_sum = entry.refund_delivery()
            if refunded_sum:
                messages.success(request, "Refunded expedited delivery, {}$".format(refunded_sum))
            else:
                messages.info(request, "No expedited delivery refunded")

        return sRes

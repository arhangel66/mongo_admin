from bson import ObjectId
from django.core import signing
from django.http import JsonResponse, HttpResponse, Http404
from django.views.generic.base import View
from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView

from mongo_admin.core.db import PyMongoHelper
from mongo_admin.main.utils import PdfHelper, EntryWrapper
from mongo_admin.services import pdf


class LeadsListView(PyMongoHelper, View):
    """
    List of leads. Return list of IDs as json response.
    Including filtering, based on correct GET parametrs. Examples:
    ?orderInfo.paymentStatus=Success
    ?orderInfo.orderId=886006&orderInfo.paymentStatus=Success
    the name of fields should be exact as in mongodb.
    """

    def get_entries(self, request):

        qs = request.GET.dict()
        if request.GET.get('like'):
            qs.pop('like')
            for key, val in qs.items():
                qs[key] = {"$regex": val}
        entries = self._entries().find(qs, {'_id': '1'})
        return entries

    def get(self, request):
        entries = self.get_entries(request)
        l_entries = []
        for entry in entries:
            l_entries.append(str(entry['_id']))
        return JsonResponse(l_entries, safe=False)


class LeadDetailView(PyMongoHelper, View):
    """
    return full info about Lead by id
    ?id=5ac5a8db680e0b64f2a74130
    """

    def get(self, request):
        entry = self._entries().find_one({'_id': ObjectId(request.GET.get('id'))})
        d_payments = self.payments_dict()
        entry['payment'] = d_payments.get(str(entry['_id']), {})
        entry['_id'] = str(entry['_id'])
        return JsonResponse(entry)


class LeadDetailSignedView(PyMongoHelper, viewsets.ViewSet):
    def get(self, request):
        encoded_string = request.GET.get('entryId')
        try:
            decoded_string = signing.loads(encoded_string)
        except:
            return Response('Error decode', status=400)

        entry_id, email = decoded_string.split('_')
        entry = self.get_entry_by_id(entry_id)
        if entry.email != email:
            return Response('Wrong email', status=400)

        if entry.get_param('orderInfo.orderId', default='') != '':
            return Response('Order was finished', status=400)

        entry.d['_id'] = entry.d['id'] = str(entry.d['_id'])

        return Response(entry.d)



class LeadSetDetailView(PyMongoHelper, View):
    """
    Allow to change any field
    ?id=5ac5a8db680e0b64f2a74130&orderInfo.notes=test_notes
    """

    def get(self, request):
        # update
        d_params = request.GET.dict()
        entries = self._entries()
        res = entries.update_one({
            '_id': ObjectId(d_params.pop('id'))
        }, {
            '$set': d_params
        }, upsert=False)
        return HttpResponse('Ok')


class RestViewSet(PyMongoHelper, viewsets.ViewSet):
    # Required for the Browsable API renderer to have a nice form.
    # serializer_class = serializers.TaskSerializer

    def get(self, request, *args, **kwargs):
        if kwargs.get('create'):
            return self.create(request)
        return super().get(request, *args, **kwargs)

    def list(self, request):
        qs = request.GET.dict()
        entries = self._entries().find(qs, {'_id': '1'})
        l_entries = []
        for entry in entries:
            l_entries.append(str(entry['_id']))
        return Response(l_entries)

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        # print(83)
        if request.GET.dict():
            return self.update(request, pk)

        entry = self._entries().find_one({'_id': ObjectId(kwargs.get('pk'))})
        d_payments = self.payments_dict()
        entry['payment'] = d_payments.get(str(entry['_id']), {})
        entry['_id'] = str(entry['_id'])
        return Response(entry)

    def update(self, request, pk=None):
        # print(94)
        d_params = request.GET.dict()
        entries = self._entries()
        res = entries.update_one({
            '_id': ObjectId(pk)
        }, {
            '$set': d_params
        }, upsert=False)

        return Response(d_params)

    def create(self, request, *args, **kwargs):
        """
        Create new entry. Can get both POST or GET.
        Convert . as sub_dict for db.
        :return:
        """
        d_params = request.POST.dict()
        if not d_params:
            d_params = request.GET.dict()

        """
        Hack for change {'taxIdRecipientInformation.confirmReceipientEmailEIN': 'asdf@asdf.com'} to
        {'taxIdRecipientInformation': {'confirmReceipientEmailEIN': 'asdf@asdf.com'}}
        """
        d_res_params = {}
        for key in d_params.keys():
            if '.' in key:
                s_first_key, s_second_key = key.split('.')
                d_res_params.setdefault(s_first_key, {})
                d_res_params[s_first_key][s_second_key] = d_params[key]
            else:
                d_res_params[key] = d_params[key]

        i_entry_id = PyMongoHelper()._entries().insert_one(d_res_params).inserted_id
        d_res_params['_id'] = str(i_entry_id)

        return Response(d_res_params, status=status.HTTP_201_CREATED)



@api_view(('GET',))
def order_status(request):
    order_id = request.GET.get('order_id')
    if not order_id:
        return Response('You have to set order id', status=400)
    entry = PyMongoHelper().get_entry_by_order_id(order_id)
    if entry:
        return Response(entry.filing_status)
    return Response('Not found', status=400)


@api_view(('GET',))
def tracking_number(request):
    order_id = request.GET.get('order_id')
    if not order_id:
        return Response('You have to set order id', status=400)
    entry = PyMongoHelper().get_entry_by_order_id(order_id)
    if entry:
        return Response(entry.get_tracking_number())
    return Response('Not found', status=400)

@api_view(('GET',))
def tracking_events(request):
    order_id = request.GET.get('order_id')
    if not order_id:
        return Response('You have to set order id', status=400)
    entry = PyMongoHelper().get_entry_by_order_id(order_id)
    if entry:
        return Response(entry.get_tracking_events())
    return Response('Not found', status=400)



@api_view(('GET',))
def tracking_events(request):
    order_id = request.GET.get('order_id')
    if not order_id:
        return Response('You have to set order id', status=400)
    entry = PyMongoHelper().get_entry_by_order_id(order_id)
    if not entry:
        return Response('Not found', status=400)

    return Response(entry.get_tracking_events())



# @api_view(('GET',))
# def order_pdf(request):
#     order_id = request.GET.get('order_id')
#     if not order_id:
#         return Response('You have to set order id', status=400)
#     entry = PyMongoHelper().get_entry_by_order_id(order_id)
#     if not entry:
#         return Response('Not found', status=400)
#
#     resp = HttpResponse(content_type='application/pdf')
#     pdf_helper = PdfHelper(entry.id)
#     resp['Content-Disposition'] = 'attachment; filename=%s' % pdf_helper.get_filename()
#     result = pdf_helper.get_file(request=request, response=resp)
#
#     return result


def order_pdf(request):
    order_id = request.GET.get('order_id')
    if not order_id:
        raise Http404()
    entry = PyMongoHelper().get_entry_by_order_id(order_id)
    if entry and entry.get_param('personalInformation.socialSecurity')[-4:] == request.GET.get('ssn'):
        resp = HttpResponse(content_type='application/pdf')
        pdf_helper = PdfHelper(entry.id)
        resp['Content-Disposition'] = 'attachment; filename=%s' % pdf_helper.get_filename()
        result = pdf_helper.get_file(request=request, response=resp)
        return result
    raise Http404()


def order_confirmation(request):
    order_id = request.GET.get('order_id')
    if not order_id:
        raise Http404()
    entry = PyMongoHelper().get_entry_by_order_id(order_id)

    if entry: #and entry.get_param('personalInformation.socialSecurity')[-4:] == request.GET.get('ssn'):
        resp = HttpResponse(content_type='application/pdf')
        resp['Content-Disposition'] = 'attachment; filename=%s' % 'order_confirmation.pdf'
        resp.content = pdf.get_order_confirmation_pdf(entry)
        return resp
        # return result
    raise Http404()


class OrderPdfView(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    def get_entry(self, request):
        order_id = request.GET.get('order_id')
        if not order_id:
            return False
        entry = PyMongoHelper().get_entry_by_order_id(order_id)
        return entry

    def get(self, request, format=None):
        """
        Return a list of all users.
        """
        entry = self.get_entry(request)
        if not entry:
            return Response('You have to set order id', status=400)

        resp = HttpResponse(content_type='application/pdf')
        pdf_helper = PdfHelper(entry.id)
        resp['Content-Disposition'] = 'attachment; filename=%s' % pdf_helper.get_filename()
        result = pdf_helper.get_file(request=request, response=resp)

        return result


def limelight_post_back(request):
    params = request.GET.dict()
    """
    http://tranquil.newagelabs.co/limelight_post_back/einLimeLightPostBack?void_refund_amount={void_refund_amount}&
    order_id={order_id}&order_total={order_total}&c1={c1}&afid={affid}&is_chargeback={ischargeback}
    void_refund_amount
    http://localhost:8000/limelight_post_back/?void_refund_amount=247&order_id=907102&ischargeback=1
    order_id
    order_total
    ischargeback
    """

    # find the entry from order_id
    entry = PyMongoHelper().get_entry_by_order_id(params['order_id'])
    if entry:
        if entry.is_refund_available(check_logs=True):
            entry.refund(sum_refund=int(params['void_refund_amount']), order_id=params['order_id'], fake=True)

        # if it's not chargebacked - then chargeback.
        if params.get('ischargeback') == '1' and not entry.is_chargebacked:
            entry.add_tag('Chargeback')
            if int(params['void_refund_amount']) == 0:
                entry.analytics_refund(entry.get_param('orderInfo.amountPaid', default=0))

    return HttpResponse('Ok')

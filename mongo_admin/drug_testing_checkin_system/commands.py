from logging import getLogger
from typing import Optional, Dict, Any

from django.conf import settings
from requests import Session, Response
from twilio.rest.api.v2010.account.message import MessageInstance

from mongo_admin.drug_testing_checkin_system.exceptions import WebCheckinLogError
from mongo_admin.services.twilio import get_twilio_client

logger = getLogger(__name__)


def test_for_drugs(
    drug_testing_phone_number: str,
    last_name: str,
    id_number: str,
    session: Optional[Session] = None,
) -> Dict[str, Any]:
    """
    Example response:

    {
        'transaction_key': '8857fd8c',
        'date': 'Tuesday, July 03, 2018  3:25AM MST',
        'required_test': 0,
        'text': 'Do not test today'
    }
    """

    if session is None:
        session = Session()
    url = "https://sentry.cordanths.com/Sentry/WebCheckin/Log"
    data = {
        'lang': 'en',
        'ivr_code': id_number,
        'last_name': last_name,
        'phone': drug_testing_phone_number,
    }

    response = session.post(url, data=data)
    response_data = response.json()[0]

    if 'error_msg' in response_data:
        raise WebCheckinLogError(response_data)

    return response_data


def send_sms(
    to: str,
    body: str,
) -> MessageInstance:
    client = get_twilio_client()
    return client.messages.create(
        from_=settings.TWILIO_FROM_PHONE_NUMBER,
        to=to,
        body=body,
    )


def send_slack_message(
    text: str,
    session: Optional[Session] = None,
) -> Response:
    if session is None:
        session = Session()
    data = {
        "text": text,
    }

    response = session.post(settings.DTCS_SLACK_WEBHOOK_URL, json=data)

    return response

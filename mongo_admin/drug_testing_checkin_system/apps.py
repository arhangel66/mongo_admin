from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class DrugTestingCheckinSystemAppConfig(AppConfig):
    name = 'mongo_admin.drug_testing_checkin_system'
    verbose_name = _("Drug Testing Checkin System")

    def ready(self):
        try:
            # noinspection PyUnresolvedReferences
            from . import signals  # noqa F401
        except ImportError:
            pass

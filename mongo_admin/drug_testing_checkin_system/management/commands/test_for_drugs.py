from argparse import ArgumentParser
from os import linesep

from django.core.management.base import BaseCommand

from mongo_admin.drug_testing_checkin_system.commands import test_for_drugs, send_sms, send_slack_message


class Command(BaseCommand):
    SMS_TEMPLATE = "Drug Testing Checkin System: " \
                   "required_test = \"{required_test}\", " \
                   "text = \"{text}\""
    SLACK_TEMPLATE = "Drug Testing Checkin System:{linesep}" \
                     "`required_test`: \"*{required_test}*\"{linesep}" \
                     "`text`: \"*{text}*\""

    help = "Drug Testing Checkin System"

    def add_arguments(self, parser: ArgumentParser):
        parser.add_argument(
            'drug_testing_phone_number',
            metavar='drug_testing_phone_number',
            type=str,
            help='Drug Testing Phone Number'
        )
        parser.add_argument(
            'last_name',
            metavar='last_name',
            type=str,
            help='Last Name'
        )
        parser.add_argument(
            'id_number',
            metavar='id_number',
            type=str,
            help='ID Number'
        )
        parser.add_argument(
            'sms_receiver_phone_number',
            metavar='sms_receiver_phone_number',
            type=str,
            help='SMS Receiver Phone Number'
        )

    def handle(self, *args, **options):
        result = test_for_drugs(
            options['drug_testing_phone_number'],
            options['last_name'],
            options['id_number']
        )

        send_sms(options['sms_receiver_phone_number'], Command.SMS_TEMPLATE.format(
            required_test=result['required_test'],
            text=result['text']
        ))
        send_slack_message(Command.SLACK_TEMPLATE.format(
            linesep=linesep,
            required_test=result['required_test'],
            text=result['text']
        ))

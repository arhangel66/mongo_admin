import environ

env = environ.Env()
ANYMAIL = {
    'MAILGUN_API_KEY': env('MAILGUN_API_KEY'),
    'MAILGUN_SENDER_DOMAIN': 'mg.tax-filing-forms.com'
}

SUPPORT_EMAIL = 'support@tax-filing-forms.com'
EMAIL_BCC = [SUPPORT_EMAIL, 'u0k3f4n4p7q7j6s1@natech.slack.com', 'arhangel662@gmail.com']

CONSTANTS = {
    "support_number": "888-675-9395",
    "support_email": SUPPORT_EMAIL,
    "domain": 'tax-filing-forms.com',
    'company_name': 'Tax Filing Forms',
    'logo_in_pdf': '/static/images/tax_sm_eagle.png',
    'email_in_pdf': SUPPORT_EMAIL
}

# ZENDESK API
ZENDESK_EMAIL = env('ZENDESK_EMAIL_TAX', default='')
ZENDESK_TOKEN = env('ZENDESK_TOKEN_TAX', default='')
ZENDESK_SUBDOMAIN = env('ZENDESK_SUBDOMAIN_TAX', default='')

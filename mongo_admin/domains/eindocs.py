import environ

env = environ.Env()
ANYMAIL = {
    'MAILGUN_API_KEY': env('MAILGUN_API_KEY'),
    'MAILGUN_SENDER_DOMAIN': 'mg.eindocs.com'
}


SUPPORT_EMAIL = 'support@eindocs.com'
EMAIL_BCC = [SUPPORT_EMAIL, 'u0k3f4n4p7q7j6s1@natech.slack.com', 'arhangel662@gmail.com']

CONSTANTS = {
    "support_number": "888-675-9395",
    "support_email": SUPPORT_EMAIL,
    "domain": 'eindocs.com',
    'company_name': 'EINDOCS',
    'logo_in_pdf': '/static/images/ein_sm.png',
    'email_in_pdf': SUPPORT_EMAIL,
}

# ZENDESK API
ZENDESK_EMAIL = env('ZENDESK_EMAIL', default='')
ZENDESK_TOKEN = env('ZENDESK_TOKEN', default='')
ZENDESK_SUBDOMAIN = env('ZENDESK_SUBDOMAIN', default='')

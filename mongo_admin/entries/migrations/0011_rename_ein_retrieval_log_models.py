from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('entries', '0010_auto_20180621_1247'),
    ]

    operations = [
        migrations.RenameModel('EntryEINRetrievalLog', 'EINRetrievalLog'),
        migrations.RenameModel('EntryEINRetrievalLogStep', 'EINRetrievalLogStep'),
    ]

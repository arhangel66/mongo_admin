# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-06-21 18:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0013_auto_20180621_1343'),
    ]

    operations = [
        migrations.AddField(
            model_name='einretrievallog',
            name='assigned_ein',
            field=models.CharField(blank=True, max_length=255),
        ),
        migrations.AddField(
            model_name='einretrievallog',
            name='assigned_legal_name',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]

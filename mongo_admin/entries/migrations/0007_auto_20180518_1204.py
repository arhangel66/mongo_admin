# -*- coding: utf-8 -*-
# Generated by Django 1.11.13 on 2018-05-18 12:04
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entries', '0006_entryeinretrievallogstep_problem_in_legacy'),
    ]

    operations = [
        migrations.AlterField(
            model_name='entryeinretrievallogstep',
            name='number',
            field=models.PositiveIntegerField(validators=[django.core.validators.MinValueValidator(1)]),
        ),
    ]

from django.contrib import admin

from mongo_admin.entries.models import Entry, SSNValidationLog
from mongo_admin.utils.django.admin import TimeStampedModelAdmin


@admin.register(Entry)
class EntryAdmin(TimeStampedModelAdmin):
    list_display = [
        'mongo_id',
    ]


@admin.register(SSNValidationLog)
class SSNValidationLogAdmin(TimeStampedModelAdmin):
    list_display = [
        'entry',
        'success',
        'message',
        'url',
        'img_data',
    ]

from django.core.validators import MinValueValidator
from django.db.models import NullBooleanField, TextField, URLField, CASCADE, ForeignKey, CharField, BooleanField, \
    PositiveIntegerField
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel

from mongo_admin.core.db import PyMongoHelper


class Entry(TimeStampedModel):
    """
    P.S. For now, only the minority of MongoDB entries
    have that relational counterpart so don't expect
    all of those to exist for now.
    """
    mongo_id = CharField(
        max_length=255,
        unique=True
    )

    class Meta:
        verbose_name = _("Entry")
        verbose_name_plural = _("Entries")

    def __str__(self):
        return f"{self.mongo_id} (#{self.pk})"


class SSNValidationLog(TimeStampedModel):
    entry = ForeignKey(
        Entry,
        related_name='%(class)ss',
        on_delete=CASCADE,
    )
    success = NullBooleanField()  # TODO: to regular bool
    message = TextField(blank=True)
    # http://stackoverflow.com/questions/10052220/advantages-to-using-urlfield-over-textfield#comment49011703_10052288
    url = URLField(
        max_length=2000,
        blank=True
    )
    img_data = TextField(blank=True)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return f"{self.success}, \"{self.message}\" (#{self.pk})"


class BaseEINLog(TimeStampedModel):
    entry = ForeignKey(
        Entry,
        related_name='%(class)ss',
        on_delete=CASCADE,
    )
    success = BooleanField(default=False)
    message = TextField(blank=True)

    class Meta:
        abstract = True
        ordering = ['-created']

    def __str__(self):
        return f"{self.success}, \"{self.message}\" (#{self.pk})"


class EINVerificationLog(BaseEINLog):
    pass


class EINRetrievalLog(BaseEINLog):
    assigned_ein = CharField(
        max_length=255,
        blank=True,
    )
    assigned_legal_name = CharField(
        max_length=255,
        blank=True,
    )

    def save_ein_in_mongo(self):
        PyMongoHelper().save_ein(self.entry.mongo_id, self.assigned_ein, self.assigned_legal_name)


class BaseEINLogStep(TimeStampedModel):
    number = PositiveIntegerField(validators=[MinValueValidator(1)])
    success = BooleanField(default=False)
    # http://stackoverflow.com/questions/10052220/advantages-to-using-urlfield-over-textfield#comment49011703_10052288
    url = URLField(max_length=2000)
    form_header = TextField()

    # TODO(@webyneter): we've got mysql in legacy production setup so say goodbuy to ArrayField etc... for the time being; introduce 'error_messages' ArrayField and migrate via Django data migrations
    ERROR_MESSAGES_LEGACY_DELIMITER = '|'
    error_messages_legacy = TextField(blank=True)  # holds a str-encoded list of strings
    # TODO(@webyneter): and again...
    problem_in_legacy = TextField(blank=True)  # holds a str-encoded JSON

    img_data = TextField()

    class Meta:
        abstract = True
        ordering = ['number']


class EINVerificationLogStep(BaseEINLogStep):
    log = ForeignKey(
        EINVerificationLog,
        related_name='steps',
        on_delete=CASCADE,
    )

    class Meta(BaseEINLogStep.Meta):
        unique_together = [
            'log',
            'number',
        ]

    def __str__(self):
        return f"Step {self.number} from log #{self.log_id} (#{self.pk})"


class EINRetrievalLogStep(BaseEINLogStep):
    log = ForeignKey(
        EINRetrievalLog,
        related_name='steps',
        on_delete=CASCADE,
    )

    class Meta(BaseEINLogStep.Meta):
        unique_together = [
            'log',
            'number',
        ]

    def __str__(self):
        return f"Step {self.number} from log #{self.log_id} (#{self.pk})"

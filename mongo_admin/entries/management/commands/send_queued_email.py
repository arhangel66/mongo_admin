# -*- coding: utf-8 -*-

from logging import getLogger

from django.core.management.base import BaseCommand

from mongo_admin.core.db import PyMongoHelper

logger = getLogger(__name__)


def send_queued_emails():
    entries = PyMongoHelper().get_queued_emails()
    for entry in entries:
        entry.send_queued_email()


class Command(BaseCommand):
    def handle(self, *args, **options):
        """ It will find all entries with not sent queuedEmail and send it if send_at > now"""
        send_queued_emails()

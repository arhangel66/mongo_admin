# -*- coding: utf-8 -*-

from logging import getLogger

from django.core.management.base import BaseCommand

from mongo_admin.core.db import PyMongoHelper
from mongo_admin.services import email

logger = getLogger(__name__)


def refund_current_in_ga():
    helper = PyMongoHelper()
    entries = helper.get_refunded_entries()
    print('found {} entries'.format(len(entries)))
    for entry in entries[:-3]:
        sum_refund = entry.get_sum_refunded()
        if sum_refund:
            res = entry.analytics_refund(sum_refund)
            if res:
                print('{} refund {} sent'.format(entry.id, sum_refund))


class Command(BaseCommand):
    help = "Datenimport der Mitarbeiter fuer Cohu:\n"

    def handle(self, *args, **options):
        # get list of leads, from mongo who has LEAD status and not have Emailed tag. + time > 30 < 45 min.
        refund_current_in_ga()

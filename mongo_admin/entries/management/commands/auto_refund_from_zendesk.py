# -*- coding: utf-8 -*-

from logging import getLogger

from django.core.management.base import BaseCommand

from mongo_admin.core.db import PyMongoHelper
from mongo_admin.services import zendesk, slack, email

logger = getLogger(__name__)


def auto_refund(zendesk_subdomain='eindocshelp'):
    # get tickets
    tickets = zendesk.get_tickets(zendesk_subdomain)
    logger.debug('get tickets - %s' % len(tickets))

    # filter tickets
    tickets = zendesk.filter_tickets(tickets)

    logger.debug('filtered tickets - %s' % len(tickets))

    for ticket in set(tickets):
        ticket.email = zendesk.get_ticket_email(ticket)

        # find entries in db
        entries = PyMongoHelper().get_any_entries_by_email(ticket.email)
        logger.info('%s found %s entries' % (ticket.email, len(entries)))

        # add founded entries to comment
        if not 'commented' in ticket.tags:
            zendesk.add_comment_with_links(ticket, entries, zendesk_subdomain)

            refunded_amount = None

            # try to get one paid entry for auto refund it
            entry = PyMongoHelper().get_paid_entry_by_email(ticket.email)
            logger.debug('Paid entry - %s' % entry)
            if entry:
                refunded_amount = entry.full_refund()
                if refunded_amount:
                    zendesk.set_refunded(ticket, zendesk_subdomain, refunded_amount)
                    email.send_refund_email(entry)
                    logger.debug("Email about refund sended")

            slack.send_message_with_links(ticket, entries, zendesk_subdomain, refunded_amount)


class Command(BaseCommand):
    def handle(self, *args, **options):
        # get list of leads, from mongo who has LEAD status and not have Emailed tag. + time > 30 < 45 min.
        auto_refund('eindocshelp')
        auto_refund('tax-filing-forms')

# -*- coding: utf-8 -*-

from logging import getLogger

from django.core.management.base import BaseCommand

from mongo_admin.core.db import PyMongoHelper
from mongo_admin.services import email

logger = getLogger(__name__)


def send_confirm_emails():
    helper = PyMongoHelper()
    entries = helper.get_fresh_entries()
    print('found {} entries'.format(len(entries)))
    for entry in entries:
        res = email.send_confirmation_pdf_email(entry)
        if res:
            entry.add_email_log('confirm pdf email')
        print('entry {} sent'.format(entry.email))


class Command(BaseCommand):
    help = "Datenimport der Mitarbeiter fuer Cohu:\n"

    def handle(self, *args, **options):
        # get list of leads, from mongo who has LEAD status and not have Emailed tag. + time > 30 < 45 min.
        send_confirm_emails()

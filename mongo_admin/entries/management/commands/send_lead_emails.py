# -*- coding: utf-8 -*-

from datetime import timedelta
from logging import getLogger

from django.core.management.base import BaseCommand
from django.utils import timezone

from mongo_admin.core.db import PyMongoHelper
from mongo_admin.main.utils import EntryWrapper
from mongo_admin.services import email

logger = getLogger(__name__)


def send_emails_to_leads():
    time_from = timezone.now() - timedelta(minutes=35)
    time_to = timezone.now() - timedelta(minutes=30)
    logger.info('from - {}, to - {}'.format(time_from, time_to))
    helper = PyMongoHelper()
    leads = helper.get_leads(time_from=time_from, time_to=time_to)
    logger.info('found {} leads'.format(len(leads)))
    for lead in leads:
        lead = EntryWrapper(lead)
        if not 'l_emailed' in lead.get_param('tags'):
            result = email.send_email_to_lead(lead)
            if result:
                logger.info('email sended to {}'.format(lead.email))
                lead.add_tag('l_emailed')
                lead.add_email_log('lead email')
                logger.info('logs saved')


class Command(BaseCommand):
    help = "Datenimport der Mitarbeiter fuer Cohu:\n"

    def handle(self, *args, **options):
        # get list of leads, from mongo who has LEAD status and not have Emailed tag. + time > 30 < 45 min.
        send_emails_to_leads()

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class EntriesAppConfig(AppConfig):
    name = 'mongo_admin.entries'
    verbose_name = _("Entries")

    def ready(self):
        # noinspection PyUnresolvedReferences
        from . import signals  # noqa

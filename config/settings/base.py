"""
Base settings to build other settings files upon.
"""

import environ

ROOT_DIR = environ.Path(__file__) - 3  # (mongo_admin/config/settings/base.py - 3 = mongo_admin/)
APPS_DIR = ROOT_DIR.path('mongo_admin')

env = environ.Env()

READ_DOT_ENV_FILE = env.bool('DJANGO_READ_DOT_ENV_FILE', default=True)
if READ_DOT_ENV_FILE:
    # OS environment variables take precedence over variables from .env
    env.read_env(str(ROOT_DIR.path('.env')))

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool('DJANGO_DEBUG', False)
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = 'US/Eastern'
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = 'en-us'
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True

# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases

# TODO:
# DATABASES = {
#     'default': env.db('DATABASE_URL'),
# }
# DATABASES['default']['ATOMIC_REQUESTS'] = True

# TODO: FIX UP THIS MESS!!!
import pymysql

pymysql.install_as_MySQLdb()
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'tranquil',
        'USER': 'tranquil_user',
        'PASSWORD': 'Staples101',
        'HOST': 'localhost',
        'USE_LIVE_FOR_TESTS': True
    }
}
DATABASES['default']['ATOMIC_REQUESTS'] = True  # noqa F405
DATABASES['default']['CONN_MAX_AGE'] = env.int('CONN_MAX_AGE', default=60)  # noqa F405

# URLSls
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = 'config.urls'
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = 'config.wsgi.application'

# APPS
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'django.contrib.humanize', # Handy template tags
    'django.contrib.admin',
]
THIRD_PARTY_APPS = [
    'crispy_forms',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'rest_framework',
    'corsheaders',
    'ckeditor',
    'anymail',
    'raven.contrib.django.raven_compat',
]
LOCAL_APPS = [
    'mongo_admin.users.apps.UsersConfig',
    'mongo_admin.entries.apps.EntriesAppConfig',
    'mongo_admin.main',
    'mongo_admin.auto_refund',
    'mongo_admin.drug_testing_checkin_system.apps.DrugTestingCheckinSystemAppConfig',
]

# CKEDITOR_BASEPATH = "/static/ckeditor/ckeditor"
CKEDITOR_CONFIGS = {
    'awesome_ckeditor': {
        'toolbar': 'Basic',
    },
}

RAVEN_CONFIG = {
    'dsn': 'https://eda0e8bd889d48c5adc4860727950cc4:16141bb82c8248a1a0c80e13b7540c22@sentry.io/1268037',
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    # 'release': raven.fetch_git_sha(os.path.abspath(os.pardir)),
}

# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# MIGRATIONS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#migration-modules
MIGRATION_MODULES = {
    'sites': 'mongo_admin.contrib.sites.migrations'
}

# AUTHENTICATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-user-model
AUTH_USER_MODEL = 'users.User'
# https://docs.djangoproject.com/en/dev/ref/settings/#login-redirect-url
LOGIN_REDIRECT_URL = 'users:redirect'
# https://docs.djangoproject.com/en/dev/ref/settings/#login-url
LOGIN_URL = 'account_login'

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
    'django.contrib.auth.hashers.Argon2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# STATIC
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = str(ROOT_DIR('staticfiles'))
# https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = '/static/'
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [
    str(APPS_DIR.path('static')),
]
# https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

# MEDIA
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = str(APPS_DIR('media'))
# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = '/media/'

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        # https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-TEMPLATES-BACKEND
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        # https://docs.djangoproject.com/en/dev/ref/settings/#template-dirs
        'DIRS': [
            str(APPS_DIR.path('templates')),
        ],
        'OPTIONS': {
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-debug
            'debug': DEBUG,
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-loaders
            # https://docs.djangoproject.com/en/dev/ref/templates/api/#loader-types
            'loaders': [
                ('django.template.loaders.cached.Loader', [
                    'django.template.loaders.filesystem.Loader',
                    'django.template.loaders.app_directories.Loader',
                ]),
            ],
            # https://docs.djangoproject.com/en/dev/ref/settings/#template-context-processors
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
# http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# FIXTURES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#fixture-dirs
FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = env('DJANGO_EMAIL_BACKEND', default='django.core.mail.backends.smtp.EmailBackend')
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
SUPPORT_EMAIL = 'support@tax-filing-forms.com'
# SUPPORT_EMAIL = 'support@eindocs.com'
EMAIL_HOST_USER = SUPPORT_EMAIL
EMAIL_HOST_PASSWORD = 'Baddude88'
EMAIL_USE_TLS = True

# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL regex.
ADMIN_URL = r'^admin/'
# https://docs.djangoproject.com/en/dev/ref/settings/#admins
ADMINS = [
    ("""Derbichev Mikhail""", 'arhangel662@gmail.com'),
]
# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# django-allauth
# ------------------------------------------------------------------------------
ACCOUNT_ALLOW_REGISTRATION = env.bool('DJANGO_ACCOUNT_ALLOW_REGISTRATION', True)
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_AUTHENTICATION_METHOD = 'username'
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_EMAIL_REQUIRED = True
# https://django-allauth.readthedocs.io/en/latest/configuration.html
# ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_EMAIL_VERIFICATION = 'none'
# https://django-allauth.readthedocs.io/en/latest/configuration.html
ACCOUNT_ADAPTER = 'mongo_admin.users.adapters.AccountAdapter'
# https://django-allauth.readthedocs.io/en/latest/configuration.html
SOCIALACCOUNT_ADAPTER = 'mongo_admin.users.adapters.SocialAccountAdapter'

# awesome-slugify
# ------------------------------------------------------------------------------
AUTOSLUG_SLUGIFY_FUNCTION = 'slugify.slugify'

# MongoDB
# ------------------------------------------------------------------------------
MONGO_HOST = '54.210.222.132'
MONGO_PORT = 27017
MONGO_APP = 'ticketingapp'
MONGO_TZ = 'US/Eastern'

#
# ------------------------------------------------------------------------------
CONSTANTS = {
    "support_number": "888-675-9395",
    "support_email": SUPPORT_EMAIL,
    "domain": 'tax-filing-forms.com',
    'company_name': 'Tax Filing Forms',
    'logo_in_pdf': '/static/images/tax_sm_eagle.png',
    'email_in_pdf': SUPPORT_EMAIL
}

# CONSTANTS = {
#     "support_number": "888-675-9395",
#     "support_email": SUPPORT_EMAIL,
#     "domain": 'eindocs.com',
#     'company_name': 'EINdocs',
#     # 'logo_in_pdf': '/static/images/ein_sm.png',
#     # 'email_in_pdf': SUPPORT_EMAIL
#     'logo_in_pdf': '/static/images/tax_sm.png',
#     'email_in_pdf': 'support@tax-filing-forms.com'
# }

# django-cors-headers
# ------------------------------------------------------------------------------
CORS_ORIGIN_WHITELIST = (
    'localhost',
    'localhost:8080',
    '127.0.0.1',
    '127.0.0.1:8080',
)

# fill_forms_irs
# ------------------------------------------------------------------------------
# TODO: envs (but keep in mind django-environ non-lazy nature)
FILLFORMSIRS_SCHEMA = 'http'
FILLFORMSIRS_HOST = 'node_fillformsirs'
FILLFORMSIRS_PORT = 8080

# LL = {
#     'login': 'totalgarcinia',
#     'password': 'NgUNHpFyZKpBAz',
#     'campaign': '319',
#     'url': 'https://oneclickawaytgc.limelightcrm.com',
#     'admin_name': 'rovin',
#     'admin_pass': 'GTG2018*',
# }
LL = {
    'url': 'http://138.197.186.28:8000/admin/{method}.php',
    # 'username': 'demoapi', # 'python1',
    'username': 'python1',
    'password': 'UTcZz2ADtvbP5A'
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# Twilio
# ------------------------------------------------------------------------------
TWILIO_FROM_PHONE_NUMBER = env('TWILIO_FROM_PHONE_NUMBER', default='')

# drug_testing_checkin_system
# ------------------------------------------------------------------------------
DTCS_SLACK_WEBHOOK_URL = env('DTCS_SLACK_WEBHOOK_URL', default='')

# lob.com sending real mail
LOB_TESTING = env('LOB_TESTING', default='True')
if LOB_TESTING == 'True':
    LOB_API = env('LOB_API_TEST', default='')
else:
    LOB_API = env('LOB_API_PROD', default='')

# FAX API
FAX_EMAIL = env('FAX_EMAIL')
FAX_PASSWORD = env('FAX_PASSWORD')
FAX_GUID = env('FAX_GUID')
FAX_TO = env('FAX_TO')
FAX_TESTING = env('FAX_TESTING')
FAX_FROM = env('FAX_FROM')
FAX_DUPLICATE = env('FAX_DUPLICATE', default='False')  # if 'True' will send same fax to our line





ZENDESK_SEARCH_WORDS = ["chargeback", "cancel", "bbb", "refund", "contact my bank", "contacted my bank", "dispute",
                        "process order", "obtained an EIN directly with the IRS", "IRS site",
                        "i did the application myself", "stop the charge", "contacted my credit card services",
                        "charge my card", "free from IRS", "cc company", "credit card company",
                        "attorney", "Lawyer", "Scam", "do not process", "REIMBURSE"]
ZENDESK_SLACK_CHANNELS = ['temp', 'ein_refund_request']

SITE_URL = 'http://tranquil.newagelabs.co/'

LOGGING = {
    "version": 1,
    "disable_existing_loggers": True,
    "root": {"level": "WARNING", "handlers": ["sentry"]},
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        }
    },
    "handlers": {
        "sentry": {
            "level": "ERROR",
            "class": "raven.contrib.django.raven_compat.handlers.SentryHandler",
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "verbose",
        },
    },
    "loggers": {
        "django.db.backends": {
            "level": "ERROR",
            "handlers": ["console"],
            "propagate": False,
        },
        "raven": {"level": "DEBUG", "handlers": ["console"], "propagate": False},
        "sentry.errors": {
            "level": "DEBUG",
            "handlers": ["console"],
            "propagate": False,
        },
        "django.security.DisallowedHost": {
            "level": "ERROR",
            "handlers": ["console", "sentry"],
            "propagate": False,
        },
    },
}
FAKE_FULL_REFUND=env('FAKE_FULL_REFUND', default='False')

SMS_REAL_SENDING = env('SMS_REAL_SENDING', default='False')
SMS_ACCOUNT_ID = env('SMS_ACCOUNT_ID', default='')
SMS_AUTH_TOKEN = env('SMS_AUTH_TOKEN', default='')

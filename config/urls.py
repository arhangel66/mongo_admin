from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views
from django.views.generic import TemplateView
from rest_framework import routers

from mongo_admin.main import views_api
from mongo_admin.main.views import EINVerificationLogsView, DiscountOrder, FaxSendView, PdfEntryNew, DelFile
from mongo_admin.main.views import ListObjects, DetailEntry, PdfEntry, ValidateEntrySSNView, EINRetrievalLogsView, \
    RefundEntry, LetterSendView
from mongo_admin.main.views_api import RestViewSet

router = routers.DefaultRouter()
router.register(r'leads', RestViewSet, base_name='leads')

urlpatterns = [
                  url(r'^$', TemplateView.as_view(template_name='pages/home.html'), name='home'),
                  url(r'^list/$', ListObjects.as_view(), name='list'),

                  url(r'^entry/validate-ssn/$', ValidateEntrySSNView.as_view(), name='validate_entry_ssn'),
                  url(
                      r'^entry/ein-retrieval-logs/$',
                      EINRetrievalLogsView.as_view(),
                      name='entry_ein_retrieval_logs'
                  ),
                  url(
                      r'^entry/ein-verification-logs/$',
                      EINVerificationLogsView.as_view(),
                      name='entry_ein_verification_logs'
                  ),
                  url(r'^entry/(?P<id>[\w.@+-]+)/$', DetailEntry.as_view(), name='detail'),
                  url(r'^entry/(?P<id>[\w.@+-]+)/pdf/$', PdfEntry.as_view(), name='pdf'),
                  url(r'^entry/(?P<id>[\w.@+-]+)/del_file/(?P<name>[\w.@+-]+)/$', DelFile.as_view(), name='del-file'),
                  url(r'^entry/(?P<id>[\w.@+-]+)/pdf/new/$', PdfEntryNew.as_view(), name='pdf-new'),
                  url(r'^entry/(?P<id>[\w.@+-]+)/refund/$', RefundEntry.as_view(), name='refund'),
                  url(r'^entry/(?P<id>[\w.@+-]+)/letter/$', RefundEntry.as_view(), name='refund'),
                  url(r'^entry/(?P<id>[\w.@+-]+)/discount/$', DiscountOrder.as_view(), name='discount'),
                  url(r'^entry/(?P<id>[\w.@+-]+)/letter/send/$', LetterSendView.as_view(), name='letter_send'),
                  url(r'^entry/(?P<id>[\w.@+-]+)/fax/send/$', FaxSendView.as_view(), name='fax_send'),

                  url(r'^getlist$', views_api.LeadsListView.as_view(), name='leads_list'),
                  url(r'^getdetails$', views_api.LeadDetailView.as_view(), name='leads_details'),
                  url(r'^docreceiving', views_api.LeadDetailSignedView.as_view({'get': 'get'}), name='leads_details'),
                  url(r'^setdetails$', views_api.LeadSetDetailView.as_view(), name='leads_modify'),

                  url(r'^getdetails$', views_api.LeadDetailView.as_view(), name='leads_details'),
                  url(r'^api/order_status/$', views_api.order_status, name='order_status'),
                  url(r'^api/order_confirmation/$', views_api.order_confirmation, name='order_pdf'),
                  url(r'^api/order_pdf/$', views_api.order_pdf, name='order_pdf'),
                  url(r'^api/tracking_number/$', views_api.tracking_number, name='tracking_number'),
                  url(r'^api/tracking_events/$', views_api.tracking_events, name='tracking_events'),

                  # Django Admin, use {% url 'admin:index' %}
                  url(settings.ADMIN_URL, admin.site.urls),

                  # User management
                  url(r'^users/', include('mongo_admin.users.urls', namespace='users')),
                  url(r'^accounts/', include('allauth.urls')),

                  # Your stuff: custom urls includes go here
                  url(r'^leads/create/$', views_api.RestViewSet.as_view({'get': 'create', 'post': 'create'}),
                      name='leads_create'),

                  url(r'^usps_tracking/', include('mongo_admin.usps_tracking.urls')),

                  url(r'^', include(router.urls)),

                  url(r'^api-auth/', include('rest_framework.urls')),
                  url(r'^limelight_post_back/', views_api.limelight_post_back, name='limelight_post_back')
              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),
    ]
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [
                          url(r'^__debug__/', include(debug_toolbar.urls)),
                      ] + urlpatterns
